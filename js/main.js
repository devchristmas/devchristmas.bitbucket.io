!(function (exports) {

    $('.s-banner .text1').typeIt({
        strings: ["Глобальное видение,", "локальная", "экспертиза"],
        breakLines: true,
        speed: 100,
        cursor: false,
    });


    $('.js-phone-mask').mask('+7(999) 999-99-99');


    function MobMenu() {
        let isOpen = false;

        $('#hamburger').on('click', (event) => {
            if (isOpen) {
                $('#mobile-menu').stop().fadeOut().removeClass('is-open');
                $('html, body').removeClass('js-noscroll');
            } else {
                $('#mobile-menu').stop().fadeIn().css('display', 'flex').addClass('is-open');
                $('html, body').addClass('js-noscroll');
            }
            isOpen = !isOpen;
            $(event.currentTarget).toggleClass('is-open');
        });
    }


    function CurrencyBlock() {
        $('.currency-item').on('click', (event) => {
            const parent = $(event.currentTarget).parents('.currency');
            parent.find('.currency-item').removeClass('is-active');
            $(event.currentTarget).addClass('is-active');
        });
    }

    function UIToggler() {
        $('.i-toggler input').on('input', (event) => {
            const toggler = $(event.currentTarget).parents('.toggler');
            toggler
                .toggleClass('is-left')
                .toggleClass('is-right');
        });

        $('.toggler > span').on('click', (event) => {
            const parent = $(event.currentTarget).parents('.toggler');
            const isLeft = $(event.currentTarget).is('.toggler > span:nth-child(1)');
            const toggler = parent.find('input');
            if (isLeft) {
                toggler.prop('checked', false);
                parent.addClass('is-left').removeClass('is-right');
            } else {
                toggler.prop('checked', true);
                parent.removeClass('is-left').addClass('is-right');
            }

        });
    }

    function ReviewsSlider() {
        $('#reviews-slider').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 626,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    function SingleSlider() {
        $('#single-slider').slick({
            slidesToShow: 1,
            fade: true,
            dots: true,
            arrows: false
        });
    }

    function BestProducts() {
        const bestSlider = $('#best-products, #analog-products')
        bestSlider.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 626,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    function SliderServices() {
        const slider = $('#slider-services');
        slider.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 626,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    function NewsSlider() {
        const slider = $('#news-slider');
        slider.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 626,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }


    MobMenu();

    SliderServices();
    BestProducts();
    NewsSlider();

    UIToggler();
    CurrencyBlock();
    ReviewsSlider();
    SingleSlider();

}(window));

function CardMap() {
    const mapEl = $('#map');
    const coords = mapEl.attr('data-coords');
    const formattedCoords = coords && coords.replace(/\s/g, '').split(',');

    let map;

    if (coords && formattedCoords) {
        map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: +formattedCoords[0], lng: +formattedCoords[1] },
            zoom: 17,
            disableDefaultUI: true
        });
    }


}