$(document).ready(function(){

	$('input[type="phone"]').mask('+7(999)99-99-99');

	// MAIN SLIDER
	$('section.layer').each(function(i){
		$('#layer-nav .layer-down').before('<button></button>');
		$('#layer-nav button:eq(0)').addClass('js-current');
	});

	const container = $('.container:eq(0)');

	function LayerNav(){
		const containerLeft = container.offset().left;
		const windowHeight = $(window).height();
		const NavHeight = $('#layer-nav').height();

		$('#layer-nav').css({
			'left': containerLeft - 50,
			'top': (windowHeight / 2) - NavHeight - 30
		});
	}
	function LayerSteps(){
		const containerWidth = container.width();
		const containerLeft = container.offset().left;
		$('#layer-steps').css({
			'left': containerLeft + containerWidth - 180
		})
	}
	LayerSteps();
	LayerNav();

	$(window).on('resize',function(){
		LayerSteps();
		LayerNav();
	});

	let currentSlide = 0;
	let slidePlaying = 0;

	$(document).on('mousewheel MozMousePixelScroll onwheel',function(e){
		if(slidePlaying == 0){
			slideShow(e);
		}
		
	});

	function slideShow(e){
		let ms = e.originalEvent.deltaY || e.detail || e.wheelDelta;
		slidePlaying = 1;
		if(ms > 0){
			// Вниз
			if( currentSlide < ($('section.layer').length - 1 )){
				currentSlide++;
			}else{
				// Бесконечная прокрутка
				currentSlide = 0;
			}
		}else{
			// Вверх
			if(currentSlide > 0){
				currentSlide--;
			}else{
				// Бесконечная прокрутка
				currentSlide = $('section.layer').length - 1;
			}
		}
		
		if($(window).width() > 1024){
			$('section.layer').removeClass('js-shown');
			$('section.layer:eq('+ currentSlide +')').addClass('js-shown');
		
			$('#layer-nav button').removeClass('js-current');
			$('#layer-nav button:eq('+currentSlide+')').addClass('js-current');
		}

		window.setTimeout(function(){ slidePlaying = 0; },500);
	}

	$('#layer-nav button').on('click',function(e){
		e.preventDefault();
		let i = $(this).index();
		currentSlide = i;
		$(this).parent().find('button').removeClass('js-current');
		$(this).addClass('js-current');

		$('section.layer').removeClass('js-shown');
		$('section.layer:eq('+i+')').addClass('js-shown');
	});

	// SLICK SLIDERS
	$('.slider').slick({
		slidesToShow:1,
		slidesToScroll:1,
		fade:true,
		arrows:false,
		adaptiveHeight:true
	});

	if( $('.slider .item').length < 10){
		$('.slider-nav .counts .max').text('0' + $('.slider .item').length)
	}else{
		$('.slider-nav .counts .max').text($('.slider .item').length)
	}

	$('.slider').on('afterChange',function(e,slick,b,a){
		$('.slider-nav .counts .current').text( parseInt(b) + 1 );
	});

	$('.slider-nav .arrow-left').on('click',function(e){
		e.preventDefault();
		$('.slider').slick('prev');
	});
	$('.slider-nav .arrow-right').on('click',function(e){
		e.preventDefault();
		$('.slider').slick('next');
	});



	// MODALS
	$('[data-modal]').on('click',function(e){
		e.preventDefault();
		let modal_id = $(this).data('modal');
		$(modal_id).fadeIn().css({'display':'flex'});
		$('body,html').css('overflow-y','hidden');
	});
	$('.ui-modal__close').on('click',function(e){
		e.preventDefault();
		$('.ui-modal').fadeOut();
		$('body,html').css('overflow-y','auto');
	});

	$(document).on('click',function(e){

		let target = e.target;

		if($(target).is('.ui-modal')){ 
			$('.ui-modal').fadeOut(); 
		}
	});


});







