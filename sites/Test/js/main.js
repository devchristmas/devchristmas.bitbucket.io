/* Main section parallax */
!(function () {

    $('.s-main').on('mousemove', (e) => {
        const curX = e.pageX
        const curY = e.pageY


        gsap.to('.s-main .item', 0.2, {x: `-${curX * 1.1}`});
        gsap.to('.s-main h3', 0.2, {x: `-${curX * 0.6}`});
        gsap.to('.s-main .item .small', 0.1, { x: curX * 0.3})
    });

}());

/* Slider */
!(function () {

    const slider = $('#slider');
    const slidesCount = $('#slider .item').length;
    const sliderNav = $(`
        <div class="slider-nav">
            <button class="prev"></button>
            <div class="count">
                <span class="current">01</span>/
                <span class="all">0${slidesCount}</span>
            </div>
            <button class="next"></button>
        </div>
    `);

    slider.slick({
        slidesToShow: 1,
        variableWidth: true,
        draggable: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1441,
                settings: {
                    sliesToShow: 1,
                    variableWidth: false
                }
            }
        ]
    });
    slider.parent().append(sliderNav);

    sliderNav.find('.prev').on('click', (e) => {
        e.preventDefault();
        $('#slider').slick('slickPrev');
    });
    sliderNav.find('.next').on('click', (e) => {
        e.preventDefault();
        $('#slider').slick('slickNext');
    });

    slider.on('afterChange', (e) => {
        const index = +slider.slick('slickCurrentSlide') + 1;
        sliderNav.find('.current').text('0' + index);
    })



}());


/* Filters , Sorting UI */
!(function () {
    const filter = $('#filter');
    const sort = $('#sort');
    const productsList = $('.products');

    let $products = [
        {
            img: "images/product.jpg",
            name: "Розы",
            desc: "Букет из 19 шоколадных роз",
            price: [2000, 1790],
            isNew: false,
            flower: "rose",
            type: "basket",
            rating: 10,
            count: 7
        },
        {
            img: "images/product.jpg",
            name: "Розы",
            desc: "Букет из 19 шоколадных роз",
            price: [2000, 1790],
            isNew: false,
            flower: "rose",
            type: "basket",
            rating: 5,
            count: 7
        },
        {
            img: "images/product.jpg",
            name: "«Пионы»",
            desc: "Букет из 19 шоколадных роз",
            price: [2000, 1790],
            isNew: false,
            flower: "pion",
            type: "box",
            rating: 10,
            count: 3
        },
        {
            img: "images/product.jpg",
            name: "«Тюльпан»",
            desc: "Букет из 19",
            price: [2000, 1790],
            isNew: true,
            flower: "tulpan",
            type: "monobuket",
            rating: 10,
            count: 11
        },
        {
            img: "images/product.jpg",
            name: "«Гарбер»",
            desc: "Букет из 19",
            price: [1500, 1790],
            isNew: true,
            flower: "garber",
            type: "buket",
            discount: 45,
            rating: 10,
            count: 15
        },
        {
            img: "images/product.jpg",
            name: "«Пионы»",
            desc: "Букет из 19 шоколадных роз",
            price: [2000, 1790],
            isNew: false,
            flower: "pion",
            type: "box",
            rating: 10,
            count: 3
        },
        {
            img: "images/product.jpg",
            name: "«Тюльпан»",
            desc: "Букет из 19",
            price: [2000, 1790],
            isNew: true,
            flower: "tulpan",
            type: "monobuket",
            rating: 10,
            count: 11
        },
        {
            img: "images/product.jpg",
            name: "«Гарбер»",
            desc: "Букет из 19",
            price: [1500, 1790],
            isNew: true,
            flower: "garber",
            type: "buket",
            discount: 45,
            rating: 10,
            count: 15
        }
    ];


    function getDataForm(form) {
        let data = {};
        if(form) {
            let sd = form.serializeArray();
            for(let f of sd) {
                if (!Array.isArray(data[f.name])) {
                    data[f.name] = [];
                    data[f.name].push(f.value);
                } else {
                    data[f.name].push(f.value);
                }
            }
            return data;
        }
    }
    function setValToInputs (slider, _min, _max) {
        if (slider) {
            const inputs = $(slider).next();
            const min = inputs.find('.min')
            const max = inputs.find('.max')
            min.val(_min);
            max.val(_max);
        }
    }


    function renderProducts () {
        const data = getDataForm(filter);
        const sortType = sort.val();
        // console.log(data, sortType);

        const filtered = $products.filter((p) => {
            if (
                data.type &&
                data.type.includes(p.type) &&
                data.flowers &&
                data.flowers.includes(p.flower) &&
                data['price-min'] <= p.price[0] &&
                data['price-max'] >= p.price[0] &&
                p.count >= data['count-min'] &&
                data['count-max'] >= p.count
            ) {
                return true;
            } else {
                return false;
            }
        });

        let sorted = filtered.sort((a, b) => {
            if (sortType === 'popular') { return a.rating - b.rating; }
            if (sortType === 'discount') { return a.discount - b.discount; }
            if (sortType === 'new') { return a - b; }
            if (sortType === 'priceDown') { return a.price[0] + b.price[0]; }
            if (sortType === 'priceUp') { return a.price[0] - b.price[0]; }

            return 0;
        });

        let result = sorted.map((product, productIndex) => {

            return $(`
                <div class="product">
                    <div class="product-img"><img src="${product.img}" alt=""></div>
                    <div class="product-name">${product.name}</div>
                    <div class="product-desc">${product.desc}</div>
                    <div class="product-price new">
                        <span class="current">${product.price[0]}</span>
                        <span class="old">${product.price[1]}</span>
                    </div>
                    <div class="product-form">
                        <div class="ui-count mini">
                            <button class="ui-count__minus"></button>
                            <input type="text" readonly value="1">
                            <button class="ui-count__plus"></button>
                        </div>
                        <a href="#" class="ui-button fill">В корзину</a>
                    </div>
                </div>
            `);
        });
        productsList.html(result);
    }
    renderProducts()


    const filterPriceSlider = $('#filter-price').get(0);
    const filterCountSlider = $('#filter-count').get(0);

    noUiSlider.create(filterPriceSlider, {
        start: [200, 5000],
        connect: true,
        range: {
            min: 200,
            max: 10000
        }

    });
    noUiSlider.create(filterCountSlider, {
        range: {
            min: 1,
            max: 101
        },
        connect: true,
        start: [1, 70]
    })


    $('.ui-slider .noUi-target').each((index, slider) => {
        slider.noUiSlider.on('slide', (e) => {
            const parent = $(slider).closest('.ui-slider');
            const min = parent.find('.min');
            const max = parent.find('.max');
            min.val(Math.floor(e[0]));
            max.val(Math.floor(e[1]));
        });
        slider.noUiSlider.on('change', (e) => {
            renderProducts($products);
        })
    })

    $('select').select2({minimumResultsForSearch: Infinity});

    $('.ui-slider .inputs input').on('input', (e) => {
        const parent = $(e.target).closest('.ui-slider');
        const slider = parent.find('.noUi-target').get(0);

        if ($(e.target).is('.min')) {
            slider.noUiSlider.set([+e.target.value, null])
        } else {
            slider.noUiSlider.set([null, +e.target.value])
        }
    });

    filter.on('change', (e) => { renderProducts() });
    sort.on('change', () => { renderProducts() })


    $('#more-products').on('click', (e) => {
        e.preventDefault();
        $products = [...$products,
            {
                img: "images/product.jpg",
                name: "«Пионы»",
                desc: "Букет из 19 шоколадных роз",
                price: [2000, 1790],
                isNew: false,
                flower: "pion",
                type: "box",
                rating: 10,
                count: 3
            },
            {
                img: "images/product.jpg",
                name: "«Тюльпан»",
                desc: "Букет из 19",
                price: [2000, 1790],
                isNew: true,
                flower: "tulpan",
                type: "monobuket",
                rating: 10,
                count: 11
            },
            {
                img: "images/product.jpg",
                name: "«Пионы»",
                desc: "Букет из 19 шоколадных роз",
                price: [2000, 1790],
                isNew: false,
                flower: "pion",
                type: "box",
                rating: 10,
                count: 3
            }
        ]
        setTimeout(renderProducts, 0);
        $(e.target).remove();
    })
}());


/* Counter */
$('.ui-count > button').on('click', (e) => {
    e.preventDefault();

    const parent = $(e.currentTarget).parent();
    const input = parent.find('input');
    const isDecrement = $(e.target).is('.ui-count__minus');
    if (isDecrement) {
        if (+input.val() > 1) {
            input.val(+input.val() - 1);
        }
    } else {
        input.val(+input.val() + 1);
    }
})