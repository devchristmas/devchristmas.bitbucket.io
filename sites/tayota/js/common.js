
$(document).ready(function(){


   /*jQuery('.service-center .scrollbarY').tinyscrollbar({
      axis:"y",
      size: 350, 
      sizethumb: 20,
      wheel:40,
      scroll:false,
   });
   jQuery('.map .list').tinyscrollbar({
      axis:"y",
      size: 350, 
      sizethumb: 20,
      wheel:40,
      scroll:false,
   });	
   */



var toggleSearch = function(){
   $('#search-button').on('click',function(){
      $('#header .mobile-search').addClass('active');
   });
   
   $(document).on('click',function(e){
    
    if($(e.target).is('.mobile-search input') == false && $(e.target).is('#search-button') == false){
      $('#header .mobile-search').removeClass('active');
    }
   })
}

toggleSearch();


/**
 * СЛАЙДЕРЫ
 */
$('.video-reviews .mini-video-gallery').slick({
   infinite: true,
   slidesToShow:2,
   swipe:false
});
$('#spec-suggestions-slider').slick({
   infinite: true,
   slidesToShow:3,
   slidesToScroll: 1,
   swipe:false,
   asNavFor: '#spec-suggestions-dots',
   responsive:[
      {
      breakpoint: 640,
      settings: {
         swipe:true,
         slidesToShow:1,
         slidesToScroll: 1

      }
    },
   ]
});
$('#spec-suggestions-dots').slick({
   asNavFor:'#spec-suggestions-slider',
   arrows:false,
   swipe:false,
   dots:true
})
$('.service-center .slider').slick({
   infinite:true,
   swipe:false,
   slidesToShow:2,
   responsive:[
      {
      breakpoint: 640,
      settings: {
         swipe:true,
         slidesToShow:1,
         slidesToScroll: 1

      }
    },
   ]
})
$('#top-slider').slick({
   infinite: true,
   swipe:false,
   slidesToShow:1,
   fade: true,
   dots:true,
   arrows:false,
   cssEase: 'linear',
   autoplay: true,
   autoplaySpeed: 5000,
   responsive:[
      {
      breakpoint: 640,
      settings: {
         swipe:true,
         slidesToShow:1,
         slidesToScroll: 1

      }
    },
   ]
});
$('.gallery-slider').slick({
   slidesToShow:1,
   dots:false,
   fade:true,
   responsive:[
      {
      breakpoint: 640,
      settings: {
         swipe:true,
         slidesToShow:1,
         slidesToScroll: 1

      }
    },
   ]
});
$('.calculator-steps.slider').slick({
   slidesToShow:1,
   variableWidth: true
});




/**
 * МАСКА ТЕЛЕФОНОВ
 */
$('input[type="phone"]').mask("+7 (999)-99-99-999");




/**
 * МОДАЛЬНЫЕ ОКНА
 */
var add_img_modal = function(){
   if( $('.img_modal').length < 1){
      $('body').prepend(`
         <div class="img_modal">
            <div class="img_wrap"><img src="#" alt=""></div>
         </div>
      `);
   }
   $(document).on('click',function(e){
      var self = e.target;
      if($(self).is('.image-gallery .img-wrap')){
         var img_src = $(self).find('img').attr('src');

         if($('.img_modal').css('display') == 'none'){
            $('.img_modal').css('display','flex');
            $('.img_modal img').attr('src',img_src);
         }else{
            $('.img_modal').css('display','none');
         }
      }

      if( $(self).is('.img_modal')){
         $('.img_modal').css('display','none');
      }
   });   
}
var use_modal = function(e){
   $("[data-show-modal]").on('click',function(e){
      e.preventDefault();
      var modal_id = $(this).data('show-modal');

      if( $(modal_id).length > 0 ){
         $(modal_id).toggleClass('show');
         $(modal_id).children('.modal__box').animate({'top':'0'},400);
         $('html').css('overflow-y','hidden');
      }
      
   });
   $(document).on('click',function(e){
      if( $(e.target).is('.modal')){
         $(e.target).removeClass('show');
         $(e.target).children('.modal__box').css('top','-100px');
         $('html').css('overflow-y','scroll');
      }
      if( $(e.target).is('button.modal__close-button')){
         var modal = $(e.target).closest('.modal');
         $(modal).removeClass('show');
         $(modal).children('.modal__box').css('top','-100px');
         $('html').css('overflow-y','scroll');
      }
      
   });
}
add_img_modal();
use_modal();





/**
 * КАРТА ЭЛЕМНТОВ МАШИН
 */
$('img[usemap]').maphilight({strokeWidth:3});




/**
 * РАБОТА С КАРТАМИ
 */
ymaps.ready(function(){

   var map = new ymaps.Map("yandex_map", {
      center: [55.734243, 37.544611], 
      zoom: 10,
      controls: []
   },{
      searchControlProvider: 'yandex#search'
   });


   myPlacemark1 = new ymaps.Placemark(map.getCenter(),{},{
      iconLayout: 'default#image',
      iconImageHref: 'images/icon-map-location.png',
      iconImageSize: [30, 42],
      iconImageOffset: [-5, -38]
   });
   myPlacemark1.events.add('mouseenter', function (e) {
      e.get('target').options.set('iconImageHref', 'images/icon-map-location-active.png'); 
      $('.map .list .list-item[data-map-mark="0"]').addClass('active');
   });
   myPlacemark1.events.add('mouseleave', function (e) {
      e.get('target').options.set('iconImageHref', 'images/icon-map-location.png'); 
      $('.map .list .list-item[data-map-mark="0"]').removeClass('active');
   });

   myPlacemark2 = new ymaps.Placemark([55.634343, 37.554511],{ },{
      iconLayout: 'default#image',
      iconImageHref: 'images/icon-map-location.png',
      iconImageSize: [30, 42],
      iconImageOffset: [-5, -38]
   });
   myPlacemark2.events.add('mouseenter', function (e) {
      e.get('target').options.set('iconImageHref', 'images/icon-map-location-active.png'); 
      $('.map .list .list-item[data-map-mark="1"]').addClass('active');
   });
   myPlacemark2.events.add('mouseleave', function (e) {
      e.get('target').options.set('iconImageHref', 'images/icon-map-location.png'); 
      $('.map .list .list-item[data-map-mark="1"]').removeClass('active');
   });
      
   
   map.geoObjects.add(myPlacemark1);
   map.geoObjects.add(myPlacemark2);

   window.marks = [],
   window.marks[0] = myPlacemark1;
   window.marks[1] = myPlacemark2;
   window.marks[0]._geoObjectComponent._options.set('iconImageHref','images/icon-map-location-active.png')
   window.marks[0]._geoObjectComponent._options.set('iconImageSize',[40,55])

});
var activeMark = function(index){
   window.marks[index]._geoObjectComponent._options.set('iconImageHref','images/icon-map-location-active.png')
   window.marks[index]._geoObjectComponent._options.set('iconImageSize',[40,55])

}
$('.map .list .list-item').on('focus',function(e){
   var mark_index = $(this).data('map-mark');
   if(mark_index!=undefined){
      activeMark(mark_index);
      $('.map .list .list-item').removeClass('active');
      $(this).addClass('active');
   }
});
$('.map .list .list-item').on('blur',function(e){
   var mark_index = $(this).data('map-mark');
   if(mark_index!=undefined){
      window.marks[mark_index]._geoObjectComponent._options.set('iconImageHref','images/icon-map-location.png')
      window.marks[mark_index]._geoObjectComponent._options.set('iconImageSize',[30, 42])
      $('.map .list .list-item').removeClass('active');
      $(this).addClass('active');
   }
});
$('.map .list .list-item[data-map-mark="0"]').addClass('active');










/* DOCUMENT LOADED */
});