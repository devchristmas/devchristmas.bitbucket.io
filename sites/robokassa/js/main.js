/* UI BUTTON INNER */
!(function () {

    $('.ui-button-inner').on('click', (e) => {
        const btn = $(e.currentTarget);
        if (!$(e.target).is('.ui-button-inner__close')){
            btn.addClass('js-open');
        }
    });
    $('.ui-button-inner__close').on('click', (e) => {
        const btn = $(e.target).closest('.ui-button-inner');
        btn.removeClass('js-open');
    });


    /*$(document).on('click', (e) => {
        if (!$(e.target).is('.ui-button-inner') && $(e.target).closest('.ui-button-inner').length === 0) {
            $('.ui-button-inner').removeClass('js-open');
            $('.header-menu, .header__search').show();
        }
    });*/
}());

/* UI SEARCH */
!(function () {

    let isOpen = false;

    $('.ui-search').on('click', (e) => {
        e.preventDefault();
        const f = $(e.currentTarget);
        if (!isOpen) {
            f.addClass('js-open');
            isOpen = true;
        }
    });


}());


/* Работа в шапке */
let header = $('.header');
let mobMenu = $('#mobile-menu');

!(function () {
    let searchIsOpen = false;

    $('.header .ui-button-inner').on('click', (e) => {
        $('.header-menu, .header__search').hide();
    });
    $('.header .ui-button-inner__close').on('click', (e) => {
        e.stopPropagation()
        if (searchIsOpen) {
            $('.header__search').show();
        } else {
            $('.header-menu, .header__search').show();
            $('.header-menu').css('display', 'flex');
        }
    })

    if (window.innerWidth > 1024) {
        $('.header__search').on('click', (e) => {
            $('.header-menu').hide();
            searchIsOpen = true;
        });
    }


    if ($(window).width() > 1024) {
        $(window).on('scroll', (e) => {
            if ($(window).scrollTop() > 120) {
                if (!header.hasClass('is-fixed')) {
                    header.addClass('is-fixed');
                    $('body').css('padding-top', 177);
                    header.find('.ui-search').removeClass('js-open');
                }
            } else {
                header.removeClass('is-fixed');
                $('body').css('padding-top', 0);
                header.find('.ui-search').removeClass('js-open');
            }
        })
    }

}());
/* мобильное меню (шапка)*/
!(function () {
    let header = $('.header');
    let hamburger = $('#hamburger');
    let search = header.find('.header__search').clone().addClass('js-open');
    let topNavItems = header.find('.header-menu > li').clone();

    let nav = header.find('.header__nav').clone();
    nav.append(topNavItems);

    let mobileLogin = header.find('.header__login-mobile, .hidden-login').clone();
    let lang = header.find('.header__lang').clone();

    let mobileNav = $('<div class="mobile-nav"></div>');
    header.append(mobileNav);

    mobileNav
        .append(nav)
        .append(search)
        .append(mobileLogin)
        .append(lang)

    hamburger.on('click', (e) => {
        e.preventDefault();
        $('body').toggleClass('js-noscroll');
        header.toggleClass('js-open');
        hamburger.toggleClass('js-open');
    });


    if (window.innerWidth <= 1024) {
        $('.header .ui-search').on('click', (e) => {
            if (e.target === e.currentTarget) {
                header.find('.header__grid > *:not(.header__search)').hide();
                header.addClass('mob-search-open');
                header.find('.ui-search').addClass('js-open');
            }

        });
    }

    $('.ui-search__close').on('click', (e) => {
        header.find('.header__grid > *:not(.header__search):not(.hidden-login)').show().css('display', 'flex');
        header.removeClass('mob-search-open');
        header.find('.ui-search').removeClass('js-open');
    });


    $('.header__login-mobile').on('click', (e) => {
        $(e.currentTarget).addClass('is-open');
    });
    $('.header .hidden-login .close').on('click', (e) => {
        header.find('.header__login-mobile').removeClass('is-open');
    });

}());

/* Accordion */
!(function () {

    $('.ui-accordion__item-caption').on('click', (e) => {
        const accordion = $(e.target).closest('.ui-accordion');
        const item = $(e.target).closest('.ui-accordion__item');
        const index = item.index();

        accordion.find('.ui-accordion__item:not(:eq('+index+'))').removeClass('js-open');
        accordion.find('.ui-accordion__item:not(:eq('+index+')) .ui-accordion__item-hidden').stop().slideUp();

        item.toggleClass('js-open');

        if (!item.hasClass('js-open')) {
            item.find('.ui-accordion__item-hidden').stop().slideUp();
        } else {
            item.find('.ui-accordion__item-hidden').stop().slideDown();
        }
    });

}());

/* UI MORE BUTTON */
!(function () {

    $('.ui-more-button').on('click', (e) => {
        const text = $(e.target).prev();
        e.preventDefault();
        $(e.target).hide();
        text.stop().animate({
            height: text.get(0).scrollHeight + 20
        }, 300);
    })

}());


/* Services slider */
$('#services-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
    dots: true,
    draggable: false
});

/* Firms slider */
$('#firms-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: true,
    responsive: [
        {
            breakpoint: 577,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 375,
            settings: {
                slidesToShow: 2,
            }
        }
    ]
});

/* Clients slider */
$('#clients-slider').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    dots: true,
    draggable: false,
});


/* Pressa about us slider*/
$('#pressa-slider').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: true,
    responsive: [
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 1,
                variableWidth: true,
            }
        }
    ]
});

/* Sertificates slider */
$('#sertificates-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    responsive: [
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 577,
            settings: {
                slidesToShow: 1
            }
        }
    ]
})


/* Cards mobile */
$('.cards.mobile').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    variableWidth: true,
    adaptiveHeight: true
})