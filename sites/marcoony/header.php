<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// Не пустим в кабинет, если создан из заказа
if(CSite::InDir("/auth/") && $_SESSION['iarga_mail_auth']=="1"){
	$_SESSION['iarga_mail_auth']="0";
	global $USER;
	$USER->Logout();
}
IncludeTemplateLangFile(__FILE__);?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
	<head>
		<link rel="shortcut icon" href="/favicon.ico" />
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="theme-color" content="#ccc">
	    <title><?=$APPLICATION->ShowTitle();?></title>
	    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/menu.css");?>
	    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/fonts.css");?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/base.css");?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/tablet.css");?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/phablet.css");?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/phone.css");?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/iarga.css");?>
		<?//$APPLICATION->SetAdditionalCSS("/inc/widget/widjet.js");?>
	    <meta name="cmsmagazine" content="90d3bb21e7721baf5d3909d423ae0f21" /> 
	    <?$APPLICATION->ShowHead();?>
	   <script id="ISDEKscript" type="text/javascript" src="/inc/widget/widjet.js"></script>
	    <script src="https://yastatic.net/share2/share.js"></script>    
	</head>
	<body>
		<?global $USER;
		$arUser = CUser::GetByID($USER->GetParam('USER_ID'))->Fetch();?>
		<?if(CSite::InDir("/index.php")):?><div class="main-block"><?endif;?>
	        <header>
			    <div class="hhead">
			        <div class="wrapper">
			            <a href="#" class="btMenu">
			                <i></i>
			            </a>
			            <div class="menu">
			                <a href="#" class="close js-close"><i></i></a>
			                <div class="box">
			                    <div class="boxB">
			                        <div class="hmenu">
			                            <img class="logoMenu" src="<?=SITE_TEMPLATE_PATH;?>/images/logo.svg" alt="" width="260px;">
<?//$_SESSION['city']=false;?>

			                            <?if($_GET['city']!="") {$_SESSION['city'] = $_GET['city'];}
										elseif(!$_SESSION['city'] && ip_city()) {$_SESSION['city'] = ip_city();}
										elseif(!$_SESSION['city']) {$_SESSION['city'] = DEF_CITY;}?>

			                            <div class="city"><a class="js-openCity"><?=$_SESSION['city'];?> <span>изменить</span></a></div>
			                            <div class="phone" style="background-image: url(<?=SITE_TEMPLATE_PATH;?>/images/call.svg);">
			                            	<?global $cont;?>
			                                <a href="tel:+<?=$cont["NUMBER"];?>" class="tel"><?=$cont["phone"];?></a>
			                                <span><?=$cont["work"];?></span>
			                            </div>
			                        </div>
									<?$APPLICATION->IncludeComponent(
										"bitrix:menu", 
										"left", 
										array(
											"ALLOW_MULTI_SELECT" => "N",
											"CHILD_MENU_TYPE" => "left",
											"DELAY" => "N",
											"MAX_LEVEL" => "1",
											"MENU_CACHE_GET_VARS" => array(
											),
											"MENU_CACHE_TIME" => "3600",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_USE_GROUPS" => "Y",
											"ROOT_MENU_TYPE" => "popup",
											"USE_EXT" => "N",
											"COMPONENT_TEMPLATE" => "left"
										),
										false
									);?>
			                    </div>
			                    <div class="fmenu" style="background-image: url(<?=SITE_TEMPLATE_PATH;?>/images/followus.svg)">
			                        <div class="socialNetwork">
										<?$APPLICATION->IncludeComponent(
											"bitrix:news.list", 
											"social", 
											array(
												"ACTIVE_DATE_FORMAT" => "d.m.Y",
												"ADD_SECTIONS_CHAIN" => "N",
												"AJAX_MODE" => "N",
												"AJAX_OPTION_ADDITIONAL" => "",
												"AJAX_OPTION_HISTORY" => "N",
												"AJAX_OPTION_JUMP" => "N",
												"AJAX_OPTION_STYLE" => "N",
												"CACHE_FILTER" => "N",
												"CACHE_GROUPS" => "Y",
												"CACHE_TIME" => "36000000",
												"CACHE_TYPE" => "A",
												"CHECK_DATES" => "Y",
												"DETAIL_URL" => "",
												"DISPLAY_BOTTOM_PAGER" => "N",
												"DISPLAY_DATE" => "N",
												"DISPLAY_NAME" => "N",
												"DISPLAY_PICTURE" => "N",
												"DISPLAY_PREVIEW_TEXT" => "N",
												"DISPLAY_TOP_PAGER" => "N",
												"FIELD_CODE" => array(
													0 => "",
													1 => "",
												),
												"FILTER_NAME" => "",
												"HIDE_LINK_WHEN_NO_DETAIL" => "N",
												"IBLOCK_ID" => "10",
												"IBLOCK_TYPE" => "shop",
												"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
												"INCLUDE_SUBSECTIONS" => "N",
												"MESSAGE_404" => "",
												"NEWS_COUNT" => "18",
												"PAGER_BASE_LINK_ENABLE" => "N",
												"PAGER_DESC_NUMBERING" => "N",
												"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
												"PAGER_SHOW_ALL" => "N",
												"PAGER_SHOW_ALWAYS" => "N",
												"PAGER_TEMPLATE" => ".default",
												"PAGER_TITLE" => "Новости",
												"PARENT_SECTION" => "",
												"PARENT_SECTION_CODE" => "",
												"PREVIEW_TRUNCATE_LEN" => "",
												"PROPERTY_CODE" => array(
													0 => "url",
													1 => "",
												),
												"SET_BROWSER_TITLE" => "N",
												"SET_LAST_MODIFIED" => "N",
												"SET_META_DESCRIPTION" => "N",
												"SET_META_KEYWORDS" => "N",
												"SET_STATUS_404" => "N",
												"SET_TITLE" => "N",
												"SHOW_404" => "N",
												"SORT_BY1" => "SORT",
												"SORT_BY2" => "ID",
												"SORT_ORDER1" => "ASC",
												"SORT_ORDER2" => "DESC",
												"STRICT_SECTION_CHECK" => "N",
												"COMPONENT_TEMPLATE" => "social"
											),
											false
										);?>
			                        </div>
			                    </div>
			                </div>
			                <div class="menuCity">
			                    <a href="#" class="backCity"></a>
			                    <div class="citySearch">
			                        <form>
			                            <input type="text" class="citySearchText selcity"  placeholder="Введите название города">
			                            <a href="#" class="citySearchBt">
			                                <svg 
			                                   style="width: 1.38rem; height: 1.38rem;"> 
			                                    <use xlink:href="#search"></use>
			                                </svg>
			                            </a>
			                        </form>
			                        <ul class="listCity load_cities">
										<?
										$cy = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>"8", "ACTIVE"=>"Y"), false, false, Array("ID", "NAME"));
										while($ct = $cy->GetNextElement())
										{
										 $arcity = $ct->GetFields();?>
										 	<li>
										 		<?if(CSite::InDir("/help/") && !CSite::InDir("/help/refund/")):?>
										 			<a href="/help/delivery-<?=ib_translit($arcity['NAME']);?>/" <?if($_SESSION['city']==$arcity["NAME"]):?>class="is-selected"<?endif;?>>
										 		<?else:?>
										 			<a href="?city=<?=$arcity['NAME']?>" <?if($_SESSION['city']==$arcity["NAME"]):?>class="is-selected"<?endif;?>>
										 		<?endif;?>
										 			<?=$arcity["NAME"];?></a></li>
										<?}?>			                        	
			                        </ul>
			                    </div>
			                </div>
			            </div>
			            <i class="bg"></i>
			            <a <?=(!CSite::InDir('/index.php'))?'href="/"':'';?> class="logo"><img src="<?=SITE_TEMPLATE_PATH;?>/images/logo.svg" alt=""></a>
			            <div class="control">
			                <div class="search">
			                    <a href="#" class="bt_search_open">
			                        <svg 
			                           style="width: 1.38rem; height: 1.38rem;"> 
			                            <use xlink:href="#search"></use>
			                        </svg>
			                    </a>
			                    <div class="hidd">
			                        <a href="#" class="close js-close-search"><i></i></a>
			                        <form action="/search/">
			                            <input type="text" class="input-search" name="qa" placeholder="поиск по сайту">
			                            <a class="bt_search submit">
			                                <svg 
			                                   style="width: 1.38rem; height: 1.38rem;"> 
			                                    <use xlink:href="#search"></use>
			                                </svg>
			                            </a>
			                        </form>

			                        <div class="tooltip_open">
			                        </div>
			                        <!--/.tooltip_open-end-->
			                    </div>
			                    <!--/.tooltip_open-end-->
			                </div>
			                <!--<a class="lk">
			                    <svg 
			                       style="width: 1.188rem; height: 1.38rem;"> 
			                        <use xlink:href="#lk"></use>
			                    </svg>
			                </a>
			                <a class="like deferred">
			                    <?include($_SERVER['DOCUMENT_ROOT']."/inc/ajax/deferred.php");?>
			                </a>-->
			                <div class="basket">
			                    <a class="btBasket">
			                        <svg 
			                           style="width: 1.56rem; height: 1.88rem;"> 
			                            <use xlink:href="#basket"></use>
			                        </svg>
			                    </a>
			                    <div class="basketBox">
			                        <div class="scrollCastom">
			                            <div class="box info-amount">
			                            	<?include($_SERVER['DOCUMENT_ROOT']."/inc/ajax/basket.php");?>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <!--/.control-->
			        </div>
			        <!--/.wrapper-->
			    </div>
			    <!--/.hhead-->
			    <div class="wrapper">
					<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"menu", 
					array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "left",
						"DELAY" => "N",
						"MAX_LEVEL" => "3",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "top_catalog",
						"USE_EXT" => "Y",
						"COMPONENT_TEMPLATE" => "menu"
					),
					false
					);?>			    
			    </div>
			</header>
			<div class="oneClickBox">
			    <div class="scrollCastom">
			        <div class="box">
			            <div class="boxB">
			                <a class="close js-close"><i></i></a>
			                <p class="title">купить в 1 клик</p>
			                <div class="price">
			                    <p class="sum_base"><?include($_SERVER['DOCUMENT_ROOT']."/inc/ajax/sum_base.php");?></p>
			                </div>
			                <div class="scroll">
			                    <div class="formBox">
			                        <form class="Valid uniform" method="post" action="/inc/ajax/one_order.php">
			                            <label class="input-box">
			                                <input type="text" class="input-text-bot" required data-msg-required="Введите Имя" name="name" placeholder="Имя" value="<?if($arUser["NAME"]):?><?=$arUser["NAME"].' '.$arUser["LAST_NAME"];?><?endif;?>">
			                            </label>
			                            <label class="input-box">
			                                <input type="text" class="input-text-bot" required data-msg="Неверная почта" data-msg-required="Введите почту или телефон" name="phone" placeholder="Телефон или e-mail" value="<?if($arUser["EMAIL"]):?><?=$arUser["EMAIL"];?><?elseif($arUser["PERSONAL_PHONE"]):?><?=$arUser["PERSONAL_PHONE"];?><?endif;?>">
			                            </label>
			                            <p class="error"></p>
			                            <input type="submit" class="button" value="отправить заказ">
			                        </form>
			                        <p class="consent">Нажимая кнопку "Отправить" вы даете свое согласие с
			                            <a href="/privacy/" terget="_blank">Политикой конфиденциальности</a></p>
			                    </div>
			                </div>
			            </div>
			            <div class="fbasket" style="background-image: url(<?=SITE_TEMPLATE_PATH;?>/images/marсony.svg)">
			                <div class="basketRegistration">
			                    <div class="sumBox">
			                        <p class="sum_base"><?include($_SERVER['DOCUMENT_ROOT']."/inc/ajax/sum_base.php");?></p>
			                        <span>стоимость доставки не включена</span>
			                    </div>
			                    <div class="text-center">
			                        <a href="#" class="link js-basket">
			                            <svg class="ar-l"
			                               style="width: 1.63rem; height: 1.19rem;"> 
			                                <use xlink:href="#arrow-right-left"></use>
			                            </svg>
			                            <span>назад в корзину</span>
			                        </a>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		<?if(!CSite::InDir("/index.php")):?><div class="main-block"><?endif;?>
			<?$APPLICATION->ShowPanel();?>
		<?if(CSite::InDir("/help/")):?>
	        <div class="breadcrumpsBg" style="background-image: url(<?=SITE_TEMPLATE_PATH;?>/images/content/breadcrumps/2.jpg);">
	            <p><?=$APPLICATION->ShowTitle(false)?></p>            
	            <div class="breadcrumpsBg--box">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"left_menu", 
						array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"COMPONENT_TEMPLATE" => "left_menu"
						),
						false
					);?>	
	                <div class="contact__list">
	                    <p class="title">напишите нам:</p>
						<?$conti = CIBlockElement::GetList(Array("SORT"=>"DESC"), Array("IBLOCK_ID"=>"18", "ACTIVE"=>"Y"), false, false, Array("ID", "NAME", "CODE", "PROPERTY_file"));
						while($cn = $conti->GetNextElement())
						{
						 $arcontect = $cn->GetFields();?>
		                    <a href="<?=$arcontect["CODE"];?>" <?if($arcontect["NAME"]!="jivosite"):?>target="_blank"<?endif;?> class="contact__list__link">
		                        <img src="<?=res($arcontect["PROPERTY_FILE_VALUE"],27,24, 1);?>" alt="">
		                    </a>						 
						<?}?> 

	                </div>
	            </div>
	        </div>	
	    <?elseif(CSite::InDir("/about/")):?>
	        <div class="breadcrumpsBg" style="background-image: url(<?=SITE_TEMPLATE_PATH;?>/images/content/breadcrumps/2.jpg);">
	            <p>о marc cony</p>
	            <div class="breadcrumpsBg--box">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"left_menu", 
						array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"COMPONENT_TEMPLATE" => "left_menu"
						),
						false
					);?>
	            </div>
	        </div>					
        <?endif;?>