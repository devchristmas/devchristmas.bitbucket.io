<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
			<?global $cont;?>
		</div><!--.main-block-end-->
		<div class="subscription <?if(CSite::InDir('/index.php')):?>ainimated<?endif;?>">
		   <div class="subscription--wrapper">
		       <p class="subscription-title h1">подписаться на рассылку</p>
		       <form method="post" class="Valid uniform" action="/inc/ajax/subscribe.php">
		           <label class="input-box">
		               <input type="text" class="input-text" placeholder="ваш e-mail" name="email" required data-msg="Неверная почта" data-msg-required="Введите почту">
		               <input type="submit" value=" " class="button-arrow">
		           </label>
		       </form>
		       <p class="subscription-textLittle"><span>Нажимая кнопку "Отправить" вы даете свое согласие с <a href="/privacy/" target="_blank">Политикой конфиденциальности</a></span></p>
		   </div>
		</div>
		<footer class="footer">
		    <div class="footer--bg">
		        <div class="footer--wrapper">
		            <a <?=(!CSite::InDir('/index.php'))?'href="/"':'';?> class="footer-logo <?if(CSite::InDir('/index.php')):?>ainimated<?endif;?>"><img src="<?=SITE_TEMPLATE_PATH;?>/images/logo.svg" alt=""></a>
		            <div class="footer-socialNetwork socialNetwork <?if(CSite::InDir('/index.php')):?>ainimated<?endif;?>">
						<?$APPLICATION->IncludeComponent(
							"bitrix:news.list", 
							"social", 
							array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"ADD_SECTIONS_CHAIN" => "N",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "N",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"CACHE_TIME" => "36000000",
								"CACHE_TYPE" => "A",
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"DISPLAY_TOP_PAGER" => "N",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"FILTER_NAME" => "",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"IBLOCK_ID" => "10",
								"IBLOCK_TYPE" => "shop",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"INCLUDE_SUBSECTIONS" => "N",
								"MESSAGE_404" => "",
								"NEWS_COUNT" => "18",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_TEMPLATE" => ".default",
								"PAGER_TITLE" => "Новости",
								"PARENT_SECTION" => "",
								"PARENT_SECTION_CODE" => "",
								"PREVIEW_TRUNCATE_LEN" => "",
								"PROPERTY_CODE" => array(
									0 => "url",
									1 => "",
								),
								"SET_BROWSER_TITLE" => "N",
								"SET_LAST_MODIFIED" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_STATUS_404" => "N",
								"SET_TITLE" => "N",
								"SHOW_404" => "N",
								"SORT_BY1" => "SORT",
								"SORT_BY2" => "ID",
								"SORT_ORDER1" => "ASC",
								"SORT_ORDER2" => "DESC",
								"STRICT_SECTION_CHECK" => "N",
								"COMPONENT_TEMPLATE" => "social"
							),
							false
						);?>
		            </div>
		            <p class="footer--copyright <?if(CSite::InDir('/index.php')):?>ainimated<?endif;?>"><?=$cont["copy"];?></p>
		            <a href="https://iarga.ru/" target="_blank" class="footer--iarga <?if(CSite::InDir('/index.php')):?>ainimated<?endif;?>">Сделано в <img class="developer__img" src="<?=SITE_TEMPLATE_PATH;?>/images/stratosfera.svg" alt=""></a>
		        </div>
		    </div>
		</footer>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/modernizr-custom.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/validate.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.maskedinput.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/nospace.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/mmenu.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/swiper.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/wow.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/paroller.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-ui.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.fancybox.min.js");?>
		<?if(CSite::InDir('/order/') || CSite::InDir('/shop/') || CSite::InDir('/catalog/')):?><?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/select.min.js");?><?endif;?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/iarga.js");?>
		<?if(!CSite::InDir('/order/') && !CSite::InDir('/shop/') && !CSite::InDir('/catalog/')):?><?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/select2.min.js");?><?endif;?>
		<?if(CSite::InDir('/catalog/')):?><?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.mlens-1.7.min.js");?><?endif;?>
		<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.mlens-1.7.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.zoom.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/svg-sprite.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/base.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/base-init.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/shop.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/func.js");?>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" >
		   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
		   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
		   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		   ym(60981718, "init", {
		        clickmap:true,
		        trackLinks:true,
		        accurateTrackBounce:true,
		        webvisor:true,
		        trackHash:true,
		        ecommerce:"dataLayer"
		   });
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/60981718" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
	
		<?if(CSite::InDir('/shop/')):?><?$APPLICATION->AddHeadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=init");?><?endif;?>
			<?if(CSite::InDir('/catalog/')):?>
	            <div class="popup" id="sale">
	                <p class="popup--title h1">узнать о снижении цены</p>
	                <form class="Valid uniform" method="post" action="/inc/ajax/lowerprice.php">
	                    <input type="hidden" name="lower" id="lower">
	                    <label class="input-box">
	                        <input type="text" class="input-text-bot" required="" maxlength="50" data-msg-required="Введите Имя" name="name" placeholder="Ваше имя" value="<?if($arUser["NAME"]):?><?=$arUser["NAME"].' '.$arUser["LAST_NAME"];?><?endif;?>">
	                    </label>
	                    <label class="input-box">
	                        <input type="text" class="input-text-bot" required="" maxlength="50" data-msg="Неверная почта" data-msg-required="Введите почту или телефон" name="phone" placeholder="Телефон или e-mail" value="<?if($arUser["EMAIL"]):?><?=$arUser["EMAIL"];?><?elseif($arUser["PERSONAL_PHONE"]):?><?=$arUser["PERSONAL_PHONE"];?><?endif;?>">
	                    </label>
	                    <p class="error"></p>
	                    <input type="submit" class="button" value="Отправить">
	                    <p class="inf">Нажимая кнопку "Отправить" вы даете свое согласие с <a href="/privacy/" terget="_blank">Политикой конфиденциальности</a></p>
	                </form>        
	            </div>
	            <div class="popup" id="size">
	                <p class="popup--title h1">сообщим о&nbsp;появлении&nbsp;размера</p>
	                <form class="Valid uniform" method="post" action="/inc/ajax/add_size.php">
				        <div class="goods--info-size goods-size">
				        </div>                    
	                    <label class="input-box">
	                        <input type="text" class="input-text-bot" required="" maxlength="50" data-msg-required="Введите Имя" name="name" placeholder="Ваше имя" value="<?if($arUser["NAME"]):?><?=$arUser["NAME"].' '.$arUser["LAST_NAME"];?><?endif;?>">
	                    </label>
	                    <label class="input-box">
	                        <input type="text" class="input-text-bot" required="" maxlength="50" data-msg="Неверная почта" data-msg-required="Введите почту или телефон" name="phone" placeholder="Телефон или e-mail" value="<?if($arUser["EMAIL"]):?><?=$arUser["EMAIL"];?><?elseif($arUser["PERSONAL_PHONE"]):?><?=$arUser["PERSONAL_PHONE"];?><?endif;?>">
	                    </label>
	                    <p class="error"></p>
	                    <input type="submit" class="button" value="Отправить">
	                    <p class="inf">Нажимая кнопку "Отправить" вы даете свое согласие с <a href="/privacy/" terget="_blank">Политикой конфиденциальности</a></p>
	                </form>        
	            </div>
	                        
	            <div class="popup popup-big" id="size-info">
	                <?include($_SERVER['DOCUMENT_ROOT']."/inc/parts/size.php");?>
	            </div>
			<?endif;?>
            <div class="popup popup-maps" id="maps" style="width:100%; height:600px;">
            </div>
            <a href="#thank" class="thank" data-fancyajax></a>
            <div class="popup" id="thank">
                <p class="popup--title h1">спасибо</p>
                <p>Ваш заказ оформлен. В ближайшее время с вами свяжется<br> менеджер.</p>
            </div>
	</body>
</html>