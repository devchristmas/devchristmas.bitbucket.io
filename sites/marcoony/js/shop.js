(function($) {
	// перехват телефонов в корзине
	$("#ORDER_PROP_2").on('keyup', function(){
		$.post('/inc/ajax/basketphone.php', {'phone':$(this).val()});		
	});

	$(".selectoffer").on("change", function(){
		$(this).closest('.goods_info').find('[data-rel]').attr("data-rel", $(this).find("option:selected").val());
		b_loaded();
	});

	$(".unitooltip").blur(function(){
		if(tooltip!=0) tooltip.abort();
		var place = $(this);
		if(place.parents("label").length > 0) place = place.parents("label").eq(0);
		if(place.next().hasClass("tooltip_open")) place.next().fadeOut();
	});

	// Change person type
	jQuery(".PERSON_TYPE .input").bind('click',function(){
		var $this = $(this).find("input");
		preloader_start($this);
		$.post(jQuery("#b_template").val()+"/props.php",{"person_type":$this.val()},function(data){
			preloader_stop($this);
			jQuery(".order_props").html(data);
			Custom.init();
			init_delivery();
			init_props();
		});
	});
	jQuery(".PERSON_TYPE input").bind('change keyup',function(){
		preloader_start(this);
		$.post(jQuery("#b_template").val()+"/props.php",{"person_type":this.value},function(data){
			preloader_stop(this);
			jQuery(".order_props").html(data);
			Custom.init();
			init_delivery();
			init_props();
		});
	});

	b_loaded();

	// Добавление в корзину
	$(document).on('click', ".to_cart", function(){
			$(this).addClass("active").next().find('input').val(1).addClass('focus');
			$(".info-amount").load("/inc/ajax/basket.php",{"add":$(this).attr('data-rel'),'num':1},b_loaded);
			$(".sum_base").load("/inc/ajax/sum_base.php",{"add":$(this).attr('rel'),'num':1}, function(){$('.btBasket').toggleClass('is-active');$('.bg').toggleClass('is-active');$('body').toggleClass('ov-hidd')});;
		return false;
	});
		
	if($("input.location").val()!="") $("input.location").click();

}(jQuery));

function b_loaded(){
	$(".cart_add").html('В корзину').removeClass('active');
	$(".amount-card input:not(.focus)").each(function(){$(this).parent().hide();});
	$(".info-amount input").each(function(){
		if($(this).attr("name")!=" "){
			var cell = $(".cart_add[data-rel="+$(this).attr("name")+"]").html('В корзине').addClass("active");
			var del =  $(".to_cart[data-rel="+$(this).attr("name")+"]").removeClass("to_cart");
			var inp = $(".amount-card[data-rel="+$(this).attr("name")+"]");
			var val = inp.show().find("input:not(.focus)").val($(this).val());
			$('input[data-rel='+$(this).attr("name")+']').val($(this).val());
		}
	});	
}

// City tooltip
function city_keyup(targ){
	if(tooltip!=0) tooltip.abort();
	var place = $(targ);
	if(place.parents("label").length > 0) place = place.parents("label").eq(0);
	if(place.next().hasClass("tooltip_open")){
		var tt = place.next();
	}else{
		var tt = $("<div class='tooltip_open'></div>").insertAfter(place).hide();
	}
	if(ttload!=false){
		ttload.abort();
		ttload = false;
	}
	ttload = $.post($(targ).attr("data-rel"),{'q':$(targ).val()},function(data){	
		tt.html(data);
		if(tt.find("a").length > 0 && $(".location").hasClass("focus")) tt.stop().fadeIn(200,function(){tt.css({'opacity':'1','display':'block'});});
		else tt.stop().fadeOut();
		tt.find("a").click(function(){
			$(this).parents(".tooltip_open").prev().val($(this).html()).keyup();
			$(this).parents(".tooltip_open").fadeOut();
		});
	});
}