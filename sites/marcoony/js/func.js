var keysave = 0;
var antiCusel = 0;
var get = new Array;
var imagepath = 'bitrix/templates/main/images';
var phoneformat = "+7 (___) ___-__-___";
var img = new Image;


function getval(num,rnd){
	if(num==undefined) return 0;
	ret = eval(num.replace(/[^0-9\.]*/,""));
	if(rnd) ret = Math.round(ret);
	if(ret==undefined) return 0;
	return ret;
}
function round(num, dec){
	var dec = 10 ^ dec;
	return Math.round(num * dec) / dec;
}
function sklon(num, form1, form2, form3){
	if(num%10==0 || num%10>4 || num==11 || num==12 || num==13 || num==14) return form1;
	else if(num%10==1) return form2;
	else return form3;
}

//  
function getAbs(val){
	return Math.abs(eval(val.replace("px","")));
}
function getClientWidth(){
  //return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientHeight;
  return window.innerWidth!=undefined? window.innerWidth : $(window).width();
}

function getClientHeight(){
  //return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.innerHeight;
  return window.innerHeight!=undefined? window.innerHeight : $(window).height();
}

function formatSumm(summ){
	var costf = "";
	cost = summ+"";
	cnt = 0;
	var i = 0;
	for(i=(cost.length-1);i>=0;i--){
		if((cnt)%3==0) costf = ' '+costf;
		costf = cost[i]+costf;
		cnt ++;
	}
	return costf;
}
function setcookie(key, val){
	$.post('/inc/ajax/setcookie.php',{'name':key,'value':val});
}

//  

$(function(){


	$(".innerlink").css({'cursor':'pointer'}).click(function(){
		if($(this).find(".innerlink").length > 0) location.href = $(this).find(".innerlink").attr("href");
		else if($(this).attr("href")!='') location.href = $(this).attr("href");
	});

	// Popup
	$(document).on('click', '.openpopup',function(){
		openpopup($(this).attr("data-rel"),$(this).attr("data-alt"));
		return false;
	});


	// 
	$(".print_it").click(function(){print();});

	//   
	$(".addpassw").on('focus',function(){
		if($(this).val() == $(this).attr("rel")){
			var newinp = $("<input type='password' name='"+this.name+"'  data-rel='"+$(this).attr('data-rel')+"' value='' class='"+this.className+"'>").insertAfter(this).focus();
			$(this).remove();
			newinp.blur(function(){
				if($(this).val()==''){
					var newinp = $("<input type='text' name='"+this.name+"'  data-rel='"+$(this).attr('data-rel')+"' value='"+$(this).attr('data-rel')+"' class='"+this.className+"'>").insertAfter(this);
					$(this).remove();
				}
			});
		}
	});

	//  pagenav Ctrl +
	if($(".b-paginator a").length > 0){
		$("body").keydown(function(e){
		  if(keysave==17 && e.which==39) location.href=$(".b-paginator .b-next").attr("href");
		  else if(keysave==17 && e.which==37) location.href=$(".b-paginator .b-prev").attr("href");
		  else keysave = e.which;
		});
	}

	if(location.href.match(/\?/)){
		var loc = location.href;
		r = loc.replace(loc.split("?")[0]+"?", "").split("&");
		for(i in r){
			v = r[i].split("=");
			get[v[0]] = decodeURIComponent(r[i].replace(v[0]+"=", ""));
		}
	}

	$(".uniform").initUnuform();


	//    hasDef  rel    
	$("input.hasDef, textarea.hasDef").each(function(){
		if($(this).val()!=$(this).attr("data-rel")) $(this).addClass("hasText");
		$(this).blur(function(){
			if($(this).val()==""){
				$(this).val($(this).attr("data-rel"));
				$(this).removeClass("hasText");
			}
		});
		$(this).focus(function(){
			if($(this).val()==$(this).attr("data-rel")){
				$(this).val("");
				$(this).addClass("hasText");
			}			
		});
	});

	// 
	$("form .submit").off('click').on('click',function(){
		if($(this).attr('name')!=undefined) $("<input type='hidden' name='"+$(this).attr('name')+"' class='tempadd'  value='1'>").appendTo($(this).parents('form'));		
		$(this).parents('form').submit(); return false;
	});
	$("form").on('submit',function(){
		$(this).find("input.repl,textarea.repl").each(function(){
			if($(this).attr("alt")==$(this).val()) $(this).val('');
		});
	});

	// phoneformat
	$(".phoneformat").phoneFormat();
});
// scroll
$.fn.showScroll = function(){
	var top = offsetPosition($(this)[0])[1];
	$('html, body').stop().animate({"scrollTop": top}, "slow");
};

$.fn.phoneFormat = function(){
	$(this).bind('keyup focus',function(event){
		var val = $(this).val();
		for(i=0;i<val.length;i++){
			if(phoneformat[i]==undefined) break;
			else if(phoneformat[i]!='_') val = repl(val,i,phoneformat[i]);
			else if(!val[i].match(/[0-9]/)){
				val = repl(val,i,'');
				break;
			}
		}
		if(event.keyCode!=8){
			if(i < phoneformat.length){
				for(i;i<phoneformat.length;i++){
					if(phoneformat[i]!='_') val = repl(val,i,phoneformat[i]); else break;
				}
			}
		}
		$(this).val(val.substr(0,(phoneformat.length -1)));		
	});
}
$.fn.initAjaxPager = function(){
	var $this = $(this);
	// Ajax loader. Works if one on page
	$(window).scroll(function(){
		if(!$("#preloadpager").length && $("#page_next").length && $(document).scrollTop()+getClientHeight()+300 >= $(document).height()){			
			$.post($("#page_next").attr("href"),{"ajax_pager":'Y'},function(data){
				if($this.parent().hasClass("masonry")){
					$this.parent().masonry("appended", $(".ajax_load", data).children().insertAfter($this.last()));
				}else{
					$(".ajax_load", data).children().insertAfter($this.last());
				}
				$this = $this.parent().children();

				$("#page_next", data).insertAfter($this.parent().next());
				$("#preloadpager").fadeOut(200,function(){$(this).remove();});

				if($(".return-top", data).length) $(".return-top", data).insertAfter($this.parent().next());
			});
			$("#page_next").remove();
		}
	});
};

//  Uniform 
$.fn.initUnuform = function(){
	// jquery uniform - needs Jquery
	$("<input type='hidden' name='antibot' value='1'>").appendTo($(this));

	$(this).find("input.repl, textarea.repl").focus(function(){if($(this).val()==$(this).attr("data-alt")) $(this).val("");});
	$(this).find("input.repl, textarea.repl").blur(function(){if($(this).val()=="") $(this).val($(this).attr("data-alt"));});
	$(this).find("input.repl, textarea.repl").keyup(function(){if($(this).val()=="" || $(this).val()==$(this).attr("data-alt")) $(this).removeClass("act"); else $(this).addClass("act");});
	$(this).find("input.repl, textarea.repl").keyup();

	$(this).find(".submit").off('click').on('click',function(){
		if($(this).attr('name')!=undefined) $("<input type='hidden' name='"+$(this).attr('name')+"' class='tempadd'  value='1'>").appendTo($(this).parents('form'));		
		$(this).parents('form').submit(); return false;
	});

	$(this).find("input").on('keypress',function(event,form){
		if(event.keyCode==13){
			$(this).parents("form").submit();
		}
	});

	
	$(this).submit(function(){

		if(antiCusel == 0){
			$(".uniform.active p.step").html('');
			$(".uniform.active .errortext").each(function(){$(this).closest(".error").removeClass("error")})
			$(this).addClass("active").removeClass("success");
			$(this).find(".element-form").removeClass("error");
			if($(this).find('.preloader').length){
				$(this).find('.preloader').show().removeClass("success");
			}
			else{
				$(this).find("p.error").show().removeClass("success");
			}
			var inps = $(this).find("input.repl, textarea.repl");
			for(i=0;i<inps.length;i++){
				$("<input type='hidden' name='"+inps.eq(i).attr("name")+"_default_value' value='"+inps.eq(i).attr('data-alt')+"' class='tempadd'>").appendTo($(this));
			}

			$("#newframe").remove();
			var newframe = $('<iframe id="newframe" name="newframe" src="'+$(this).attr('action')+'"></iframe>').appendTo("body").hide();
			var newform = $(this).attr("target","newframe").attr("method","post").attr("enctype","multipart/form-data");

		
			
			//newform.submit();

			newframe.bind('load',function(){
				$(".tempadd").remove();
				var data = $(this).contents().find('body').html();
				if (typeof handler == 'function') {
					handler(data);
				}
				if(data.match("step") && $(".uniform.active").find(".step").length){
					var mat = data.match(/step ([0-9]+):([0-9]+)/);
					$(".uniform.active").find(".skipadr").remove();
					$(".uniform.active").find(".skip_adr").remove();
					$(".uniform.active").append($("<input type='hidden' name='skip' class='skip'>").val(mat[2]));
					$(".uniform.active").append($("<input type='hidden' name='skip_adr' class='skip_adr'>").val(mat[1]));
					$(".uniform.active p.step").html($(".uniform.active p.step").html()+'<br>'+data.replace('step','').replace('error',''));
					$(".uniform.active p.active").html('');
					setTimeout(function(){$(".uniform.active").removeClass('active').submit();},2000);
				}else if(data.match("error")){
					setTimeout((function(data_){return function(){
						var mat = data.match(/errorblock/);
						if(mat){
							dataArr = data.replace("error ","").split(/errorblock:[0-9a-zA-Z_\-]+/);
							codesArr = data.replace("error ","").match(/errorblock:[0-9a-zA-Z_\-]+/g);	
							for(i in dataArr){
								if(codesArr[i]!=undefined){
									code = codesArr[i].replace('errorblock:','');
									$code = $(".uniform.active").find("input[name='"+code+"'], textarea[name='"+code+"']");
									if($code.length){
										$code.parent().addClass("error").find(".errortext").html(dataArr[i]);
										data = data.replace(dataArr[i]+codesArr[i],"");
									}else{
										data = data.replace(dataArr[i],"");
									}
								}
							}
							if($(".uniform.active .incorrect-password div.error").length){
								$(".uniform.active .incorrect-password").show().find('div.error').html(data.replace("error ","")).fadeIn();
							}
							else{
								$(".uniform.active p.error").html(data.replace("error ","")).fadeIn();
								$(".uniform.active .performed, .uniform.active .preloader").hide();
							}
							$(".uniform.active").removeClass("active");
						}else{
							if($(".uniform.active .incorrect-password div.error").length){
								$(".uniform.active .incorrect-password").show().find('div.error').html(data.replace("error ","")).fadeIn();
							}
							else{
								$(".uniform.active p.error").html(data.replace("error ","")).fadeIn();
								$(".uniform.active .performed, .uniform.active .preloader").hide();
							}
						}
					}})(data),200);			
				}else  if(data.match("success")){
					setTimeout((function(data_){return function(){
						if(data.match("refresh")) history.go(0);
						else if(data.match("redirect")){
							var mat = data.split(":");
							if(mat) location.href = (mat[1]);
						}else if(data.match("alert")){
							alert("success");
						}else if(data.match("closeit")){
							$(".uniform.active").html("<p>"+data.replace("success ","").replace("closeit","")+"</p>");
							setTimeout(function(){$(".popup_bg").click();},3000);
						}else{
							if(data.match("clear")){
								data = data.replace("clear", "");
								$(".uniform.active input, .uniform.active textarea").each(function(){
									if($(this).attr("type")=="text") $(this).val("");
									else if($(this).is("textarea")) $(this).val("");
									else if($(this).attr("type")=="checkbox") $(this).arrt("checked", false);
									else if($(this).attr("type")=="radio") $(this).arrt("checked", false);
								});
							}
							if(data.match("nodelete")){
								if($('.performed').length){
									$(".uniform.active .performed").html(data.replace("success ","").replace("nodelete","")).show();
									$(".uniform.active .error, .uniform.active .preloader").hide();
								}
								else{
									$(".uniform.active p.error").html(data.replace("success ","").replace("nodelete","")).addClass("success");
								}
							}else{
								$(".uniform.active").html("<p>"+data.replace("success ","")+"</p>");
							}
							//$(".uniform.active p.error").html(data.replace("success ","")).addClass("success").fadeIn();
						}
						$(".uniform.active").removeClass("active");
					}})(data),200);		
				}else{
					$(".uniform.active p.error").html("").fadeIn();
					$(".uniform.active").removeClass("active");
				}
			});
		}
		//return false;
	});
	$(window).resize(function(){
		$(".popup").eq(0).height(getClientHeight());
	});
}

//     Cusel Submit
$(".cusel").on('mouseup',function(){antiCusel = 1;});
$(".cusel").on('click',function(){setTimeout(function(){antiCusel = 0;},100);});

//        [0] -  [1] - 
function offsetPosition(element) {
	if(element!=undefined){
		var offsetLeft = 0, offsetTop = 0;
		do {
			offsetLeft += element.offsetLeft;
			offsetTop  += element.offsetTop;
		} while (element = element.offsetParent);
		return [offsetLeft, offsetTop];
	}
}


//  
$(function(){$(".popup").eq(0).click(function(ev){$(".popup_bg").click();});});
function openpopup(adr,vals){
	if($(".popup").length  < 1){
		$("<div class='popup'></div>").appendTo($('body')).css({'width':'100%','display':'none','overflow':'auto'}).click(function(){$('.popup_bg').click();});
	}
	$(".popup").fadeIn().css({'z-index':'301'}).html("<img src='/"+imagepath+"/preloader.gif'>");
	var popup_style = {'position':'fixed','top':'0px','width':'100%','height':$(document).height(),'margin-bottom':'100px','z-index':'300','background':'#000','opacity':'0.5'};
	if(!$(".popup_bg").length) $("<div class='popup_bg'>").appendTo($('body')).fadeIn().css(popup_style).click(function(){
			$(".popup").fadeOut(); 
			$('body').css('overflow','auto'); 
			$(this).fadeOut(function(){$(this).remove();});
	});
	$.post("/inc/popups/"+adr+".php",vals,function(data){		
		$(".popup").html(data).height(getClientHeight()).css('top',$(document).scrollTop());
		$(".popup div").eq(0).fadeIn().css({'margin1':'40px auto'}).click(function(ev){ev.stopPropagation;});
		$('body').css('overflow','hidden');
		$(".popup .uniform").initUnuform();
		$(".popup div").eq(0).click(function(ev){ev.stopPropagation();});
		$(".popup .close, .popup .x").click(function(){$(".popup_bg").click(); return false;});
		
		// 
		$(".popup input.repl, .popup textarea.repl").focus(function(){if($(this).val()==$(this).attr("rel")) $(this).val("");});
		$(".popup input.repl, .popup textarea.repl").blur(function(){if($(this).val()=="") $(this).val($(this).attr("rel"));});

		// . 
		if(typeof('popup_opened')=='function') popup_opened();

		// New position
		var top = $(document).scrollTop();
		p_cont = $(".popup div").eq(0).css({'z-index':'302'});
		p_cont.css({'margin-left':($(document).width()-p_cont.width())/2});
		if(p_cont.height() + top > $(document).height()) top = $(document).height() - p_cont.height() - 50;
		else if(p_cont.height() < getClientHeight() - 100) p_cont.css({'position':'fixed','top':((getClientHeight()-p_cont.height())/2)+'px'});
		else $(".popup").css({'position':'absolute','top':top+'px'});

		mleft = (getClientWidth() - p_cont.width())/2;
		if(mleft < 0) mleft = 0;
		//p_cont.css({'margin-left':mleft+'px'});
	});
}


//  
function repl(str,num,symb){
	str1 = str.substr(0,num);
	str2 = str.substr(num+1);
	return str1+symb+str2;
}

function easyScroll(top2){
	var top1 = $(document).scrollTop();
	var move = top2-top1;
	if(move > 0){
		for(i=0;i<=move;i+=10) setTimeout((function(i_){return function(){$(document).scrollTop($(document).scrollTop()+10);}})(i),i/2);
	}else{
		move = move * -1;
		for(i=0;i<=move;i+=10) setTimeout((function(i_){return function(){$(document).scrollTop($(document).scrollTop()-10);}})(i),i/2);
	}
}


//выравнивает высоту всех элементов в ряду(ряд определяется относительно offset().top) для элементов с классом selectorClass
//если нет возможности добавить селектор, область видимости можно ограничить, задав родительский для элоементов контэйнер $container
//чтобы скрипт сработал, элмент должен быть блочным
function heightBeautifizer(selectorClass, $container){
	if(typeof($container) != "object"){
		$container = $(document);
	}
	if($(selectorClass, $container).length > 0){
		var $smallgroup = [];
		var $smallgroupsheights = [];
		var $maxheight = 0;
		var $lastoffset = $(selectorClass, $container).eq(0).offset().top;
		$(selectorClass, $container).each(function(i){
			var $myoffset = Math.floor($(this).offset().top);
			if($(selectorClass, $container).length-1 == i){
				$smallgroup.push($(this));
				$smallgroupsheights.push($(this).height());
			}
			if($myoffset != $lastoffset || $(selectorClass, $container).length-1 == i){
				$maxheight = Math.max.apply(0,$smallgroupsheights);
				$.each($smallgroup,function(){
					$(this).height($maxheight);
				});
				$maxheight = 0;
				$smallgroup = [];
				$smallgroupsheights = [];
			}
			$smallgroup.push($(this));
			$smallgroupsheights.push($(this).height());
			
			$lastoffset = $myoffset;

		});
	}
}