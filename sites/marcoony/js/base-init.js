$(function() {
	$('.Valid').each(function(){
        $.Valid($(this));
    });
    $.mainSlider();
	$.catalogSlider();
    $.catalogSliderBig();
    $.masked();
    $.newsSlider();
    setTimeout(function () {
        $.goodsSlider();
    }, 200)
    if($('.no-touchevents').length) {
        $.zoomBox();        
    };
    $.fancyImages(
        $('[data-fancybox="images"]:not(.swiper-slide-duplicate)')
    );
});