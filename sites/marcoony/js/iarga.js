var tooltip = 0;

(function($) {


	//выбор города
	$('.selcity').on('keyup', function(){
		$('.load_cities').load('/inc/ajax/cities.php', {'name':$(this).val()});
	});
	//оформление заказа
	$(document).on('click','.click_deiv', function() {
		var id = $(this).attr("rel");
		$("#ID_DELIVERY_ID_"+id).click();
	});

		//сортировка товаров
		//$(".filter-sort .filters").on("click", function(){
		$(document).on('click','.filter-sort .filters', function() {
			var chek = $(this).parents('.filter-hidd').find('input:checked');
			$(this).closest('.filter-sort').find('.filter-title').removeClass('visible');
			if(chek.val()){
				location.href=chek.val();
			}
		});
		//сбросим сортировку
		$(document).on('click','.filter-sort .rem', function() {
			location.href="?sort_by=SORT&sort_order=DESC";
		});

		$('.filter-sort').each(function(){
			var chek = $(this).find('input:checked');
			if(chek.length==1){
				$(this).closest('.filter-sort').find('.filter-title').addClass('visible');
				$(this).closest('.filter--box').find('.filter-title').removeClass('visibleOne');
			}
			else{
				$(this).closest('.filter-sort').find('.filter-title').removeClass('visible');
				//$(this).closest('.filter--box').find('.filter-title').addClass('visibleOne');
			}
		});
	//показать еще товары
	$(document).on('click','#ajax_product', function() {
		$.post($(this).attr('rel'), {ajax:"Y"}, function(data) {
			$("no_sorting").remove();
			$(".sstring_lod").remove();
			$(".show-more").remove();
			$(".main-ui-pagination").remove();
			$('.ajax_load').append($(data).find('.ajax_load').html());
			$('.ajax_loads').append($(data).find('.ajax_loads').html());
		});
	});
	//запишем выбранный салон
	$(document).on('click','.click_salon', function() {
		$("#ORDER_PROP_6").val($(this).attr("rel"));
	});

	a_loaded();
	//Добавление товаров в избранное
    $(document).on('click', '.js-like', function(){
        $(".deferred").load("/inc/ajax/deferred.php",{"add":$(this).attr('data-rel'),"act":1},a_loaded);
		return false;
    });
	//Удаление товара из избранного
	$('.del_aside').click(function(){
		$(".deferred").load("/inc/ajax/deferred.php",{"add":$(this).attr('data-rel'),"act":0});
		setTimeout(function(){location.reload()}, 1000);
	
	});  
	//выбрать размер
    $(document).on('change', '.offer_size', function(){
    	var size = $(this).val();
    	var elem = $(this).attr("rel");
    	$('.cart_add').removeClass('disabled');
    	$('.cart_add').addClass('to_cart');
    	$('.cart_add').attr("data-rel",size);
    	$('#pres_add').attr("data-rel",size);

    	$(".size_error").remove();
    	b_loaded();

    });
    $(document).on('click', '.cityorder', function(){
    	$(".dropdown-field").val($(this).attr('rel'));
    	$(".bx-ui-sls-fake").val($(this).attr('rel'));
    	submitForm();
    	$(".select2-container").remove();
    	return false   
    });


	//если товар добавлен , показать корзину
	$(document).on('click', '.cart_add:not(.disabled)', function(){
		$(".btBasket").click();
	});
	//Добавить товар в форму "Узнать о снижении цены"
	$(document).on('click', '.saleInfo', function(){
		$("#lower").attr("value",$(this).attr('rel'));
		return false
	});
	//Добавить заявку "Сообщим о появлении размера"
	$(document).on('click', '.bell', function(){
		$(".goods-size").load("/inc/ajax/good_size.php",{"add":$(this).attr('rel'),'size':$(this).attr('size'),'id':$(this).attr('id')});
		//$(".addsize").attr("value",$(this).attr('size'));
		//$(".idsize").attr("value",$(this).attr('rel'));
		return false
	});
	//подгрузим салоны в форму наличия
	$(document).on('change', '.option_l', function(){
		var offer = $("#pres_add").attr('data-rel');
			elem = $(".pres_elem").val();
		$(".checkCont").load("/inc/ajax/salon_check.php",{"city":$(this).val(),"elem":elem, "offer":offer}, function(){
			
			var salon = $(".click_salons[checked]").attr("rel");
				
				offer = $("#pres_add").attr('data-rel');
			$(".size-elem").load("/inc/ajax/element_offer.php",{"salon":salon,"add":elem, "offer":offer});
			
		});

		return false
	});
	//подгрузим салоны в форму наличия
	$(document).on('click', '#pres_add', function(){
		//$(".pres_elem").attr("value",$(this).attr('rel'));
		var	offer = $(this).attr('data-rel');
			element = $(this).attr('rel')
		$(".checkCont").load("/inc/ajax/salon_check.php",{"elem":element, "offer":offer},
		function(){
				var salon = $(".click_salons[checked]").attr("rel");
				$(".size-elem").load("/inc/ajax/element_offer.php",{"salon":salon,"add":element, "offer":offer});
		});

		return false
	});
	//подгрузим салоны по клику на размер zise_add
	$(document).on('click', '.zise_add', function(){
		var offer = $(this).attr("rel");
			element = $(".pres_elem").val();
			city =  $(".click_salons[checked]").attr("rel");
		$(".checkCont").load("/inc/ajax/salon_check.php",{"city":$(".option_l").val(),"elem":element, "offer":offer});
	});


	$(document).on('click', '.click_salons', function(){
		var elem = $(".pres_elem").val();
			salon = $(this).attr('rel');
			offer = $("#pres_add").attr('data-rel');
		$(".size-elem").load("/inc/ajax/element_offer.php",{"salon":$(this).attr('rel'), "add":elem, "offer":offer});
	});
    // Убирает невидимый блок онлайн консультанта
    function jivo_onClose(){ document.getElementById('jivo-iframe-container').setAttribute('style', 'right: -999px;'); }
    //поиск по каталогу
    $(document).on('keypress', '.input-search', function (){
    	$(".tooltip_open").load("/inc/ajax/tooltip.php",{"qa":$(this).val()});
    });
    //Выберите размер
    $(document).on('click', '.cart_add.disabled', function (){
		var div = $('.cart_block').append('<div class="size_error">Выберите размер</div>');
    	// if ($(window).width() > 1000) {
		// 	var div = $('.cart_block').append('<div class="size_error">Выберите размер</div>');
		// } else {
		// 	var div = $('.goods--info-size').append('<div class="size_error">Выберите размер</div>');
		// }
    });   
}(jQuery));
	function a_loaded(){
		$(".deferred input").each(function(){
			var cell = $(".js-like[data-rel="+$(this).attr("name")+"]").addClass("is-active");
			var inp = $(".js-like[data-rel="+$(this).attr("name")+"]");
			var val = inp.show().find("input:not(.focus)").val($(this).val());
			$('input[data-rel='+$(this).attr("name")+']').val($(this).val());
		});	
	}  
		$(document).on('click', '.filter-titles', function(){
		    $.offsetBgFilter();
		    $(this).closest('.filter--box').toggleClass('active').siblings('.filter--box').removeClass('active');
		    if( $(this).closest('.filter--box').hasClass('active')){
		        $('.bgFiler').addClass('hover');
		    }else {
		        $('.bgFiler').removeClass('hover');
		    }
		    return false
		});
function preloader_start(){
	var pretout = $("body").append("<div class='preloader'><div class='item-1'></div><div class='item-2'></div><div class='item-3'></div><div class='item-4'></div><div class='item-4'></div></div>");
}
function preloader_stop(){
	$(".preloader").remove();
}
function toTranslit(text) {
	        return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
	        function (all, ch, space, words, i) {
	            if (space || words) {
	                return space ? '-' : '';
	            }
	            var code = ch.charCodeAt(0),
	                index = code == 1025 || code == 1105 ? 0 :
	                    code > 1071 ? code - 1071 : code - 1039,
	                t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh',
	                    'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
	                    'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh',
	                    'shch', '', 'y', '', 'e', 'yu', 'ya'
	                ]; 
	            return t[index];
	        });
}

/*function setDocHeight() {
	document.documentElement.style.setProperty('--vh', `${window.innerHeight/100}px`);
};
window.addEventListener('resize', function () {
	setDocHeight();
});
window.addEventListener('orientationchange', function () {
	setDocHeight();
});*/
$(window).on('resize touchmove', function () {
	var mob_height = window.innerHeight;
	$('.basketBox .scrollCastom').height(mob_height);
	// adjust heights basing on window.innerHeight
	// $(window).height() will not return correct value before resizing is done
});

function ajaxFilter(targ){
	preloader_start();
	$("#newframe1").remove();
	$form = $(targ).closest("form");
	var adr = ("?"+$form.serialize()+'&ajax_filter=1&ajax=y');
	$.post("/inc/ajax/filterprovider.php", {'adr':adr, 'base':location.href},function( data ) {
		var splitarr = data.split('\'SEF_SET_FILTER_URL\':\'');
		var splitarr1 = splitarr[1].split('\'');
		var link = splitarr1[0];

		linkarr = link.split('/');
		if(linkarr[2].match('-is-')) link = link.replace('/catalog/', '/catalog/all/');
		window.history.pushState('2','Title', link);
		$.post(link,{'ajax':'1'},function(data){
			$("<div id='ajax_load'></div>").appendTo($('body')).html(data);
			$(".ajax_load").eq(0).html($('#ajax_load').find(".ajax_load").html());
			$('#ajax_load').find(".bx_filter_param_text").each(function(){
				var $span = $(this).find("span[data-role]");
				if($span.length){
					var role = $span.attr("data-role");
					var $targ = $("span[data-role='"+role+"']");
					$targ.html($span.html());
					if(parseInt($span.html()) == 0) $targ.closest("label").addClass("disabled");
					else $targ.closest("label").removeClass("disabled");
				}
			});
			$('#ajax_load').remove();		
			preloader_stop();
		});

	});
}