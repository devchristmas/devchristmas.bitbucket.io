$.uiSlider  = function () {
    var $el = $('.uiSlider');
    if (!$el.length) return;

    var $el = $('.uiSlider');
    if (!$el.length) return;

    $el.each(function(){

         var $this = $(this),
             $maxVal = $this.attr('data-maxVal'),
             $minVal = $this.attr('data-minval'),
             $dmax = $this.attr('data-max'),
             $dmin = $this.attr('data-min'),             
             $thisMinCost = $this.closest('.ui').find('input.minCost'),
             $thisMaxCost = $this.closest('.ui').find('input.maxCost');
         $this.slider({
            animate: 'slow',
            range: true,    
            values: [$dmin, $dmax],
            min: $minVal-1,
            max: $maxVal,
            stop: function(event, ui) {

                if($('.ui-widget-header').attr("style")){
                    var out=[];
                    $('.ui-state-default').each(function(index, element) {
                        var style = $(this).attr("style");
                        if(index==1){
                            var re = /[0-9/]+/;
                            proc = style.match(re);
                            style = 100 - proc[0];
                            right = 'right: '+style+'%;';
                        }
                        else{
                            left = style;
                        }                   
                    });
                    out = right+' '+left;
                    console.log(left);
                    $('.bx_ui_slider_pricebar_VD').attr("style",out);
                    $('.bx_ui_slider_pricebar_V').attr("style",out);
                    $('.bx_ui_slider_handle.left').attr("style",left);
                    $('.bx_ui_slider_handle.right').attr("style",right);
                }

                             
                $thisMinCost.val($this.slider('values',0));
                $thisMaxCost.val($this.slider('values',1));
                smartFilter.keyup(this);
                var mins = $this.slider('values',0);
                    maxs = $this.slider('values',1)
                $this.closest('.filter-price').find('.sel').html(mins + ' руб. -' + maxs+' руб.');
                $this.closest('.filter-price').find('.filter-title').addClass('visible');
                
            },
            slide: function(event, ui){
                $thisMinCost.val($this.slider('values',0));
                $thisMaxCost.val($this.slider('values',1));

            }          
        });         
        $thisMinCost.change(function(){
            var value1=$thisMinCost.val();
            var value2=$thisMaxCost.val();
            if(parseInt(value1) > parseInt(value2)){
                value1 = value2;
                $thisMinCost.val(value1);
            }
            $this.slider('values',0,value1);  
            $this.closest('.filter-price').find('.sel').html(value1 + ' руб. -' + value2+' руб.');
            $this.closest('.filter-price').find('.filter-title').addClass('visible');
        });
        $thisMaxCost.change(function(){
            var value1=$thisMinCost.val();
            var value2=$thisMaxCost.val();
            //if (value2 > $maxVal) { value2 = $maxVal; $thisMaxCost.val($maxVal)}
            if(parseInt(value1) > parseInt(value2)){
                value2 = value1;
                $thisMaxCost.val(value2);
            }
            $this.slider('values',1,value2);
            $this.closest('.filter-price').find('.sel').html(value1 + ' руб. -' + value2+' руб.');
            $this.closest('.filter-price').find('.filter-title').addClass('visible');
        });


    });

    // фильтрация ввода в поля
    $('.valSl').keypress(function(event){
        var key, keyChar;
        if(!event) var event = window.event;

        if (event.keyCode) key = event.keyCode;
        else if(event.which) key = event.which;

        if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
        keyChar=String.fromCharCode(key);

        if(!/\d/.test(keyChar)) return false;

    });
};
(function($) {
    //2 колонки
    $('.column').each(function(){
        if($(this).height() > 344) {
            $(this).addClass('column2')
        }    
    });
    $('.column2').each(function(){
        if($(this).height() > 344) {
            $(this).addClass('column3')
        }    
    });
    //header mobile 
    $('.logo').clone().addClass('mob').prependTo($('header'))
    //header catalog
    $('.menuCatalog').clone().attr('id', 'mmenu').prependTo($('.bmenu'));
    $('#mmenu .menu-catalog').each(function(){
        $(this).closest('.secindLvl').find('.title').replaceWith(function(index, oldHTML){
            $("<span>").html(oldHTML);
        });
    });
    //Меню
	$("#mmenu").mmenu({
		extensions: [ "theme-white" ],
		navbar: {
			title: "",
            offCanvas: false
		}
	});
    $('.menu .hmenu').after($('#mmenu'));
    $('#mm-0').removeClass('mm-page').removeClass('mm-slideout').removeAttr('id');
    
    //filter mobile
    if($('.filter').length){
        if($(window).width() <= 1000){
            
            $('.breadcrumbs').append('<ul class="filter sortMob"></ul><div class="filterMobile"><a href="#" class="filter--bt">фильтр</a><div class="mmFilter"/></div>');
            $('.sortMob').append($('.filter-sort').clone());
            $('.mmFilter').append($('.filter--wrapper'));
            $(".mmFilter").mmenu({
                extensions: ["position-right", "position-front"],
                navbar: {
                    title: "фильтр",
                    offCanvas: false,
                }
            }); 
            $('.mmFilter').prepend('<a class="close js-close-filter"><i/></a>');
            $('.mmFilter').wrap('<form name="_form" action="/catalog/" method="get" class="smartfilter"/>');
        }
        $.uiSlider();
        $('.filter').append('<div class="bgFiler"/>');
        $.offsetBgFilter = function() {
            var offsetTop = $('.filter').offset().top,
                heightFilter = $('.filter').height(),
                winTop = $(window).scrollTop(),
                offsetBg = offsetTop + heightFilter - winTop;
            $('.bgFiler').css('top', offsetBg);
        }
        $.offsetBgFilter();
        $(window).on('resize scroll', function(){
            $.offsetBgFilter();
        });
    };
    $(document).on('click', '.filter--bt', function(){
        $('.mmFilter').toggleClass('is-active');
        $('.bg').toggleClass('is-active');
        $('body').toggleClass('ov-hidd');
        $('html').toggleClass('ov-hidd');
        return false
    });
   $(document).on('click', '.sortMob .filter-title', function(){
       $(this).closest('.filter-sort').toggleClass('active');
       return false
   });
    $(document).on('click', '.js-close-filter', function(){
        $('.mmFilter').removeClass('is-active');
        $('.bg').removeClass('is-active');
        $('body').removeClass('ov-hidd');
        $('html').removeClass('ov-hidd');
        return false
    });
    $(document).on('click', '.js-like', function(){
        $(this).toggleClass('is-active');
    });
}(jQuery));

$(document).on('click', '.backCity', function(){
    $(this).closest('.menuCity').removeClass('is-open');
    return false
});
//$(function(){
//    $('.filter--box').hover(function(){
//        $('.bgFiler').addClass('hover');
//    }, function() {
//        $('.bgFiler').removeClass('hover');
//    });
//});
$(document).on('click', '.bx_filter .filter-title', function(){
    $.offsetBgFilter();
    $(this).closest('.filter--box').toggleClass('active').siblings('.filter--box').removeClass('active');
    // if( $(this).closest('.filter--box').hasClass('active')){
    //     $('.bgFiler').addClass('hover');
    // }else {
    //     $('.bgFiler').removeClass('hover');
    // }
    return false
});

$(document).on('click', '.filter--box .button', function(event){
    var $this = $(this);
    var $container = $this.closest('.filter--box');
    if ($container.hasClass('active')) {
        $container.removeClass('active');
    }
});

$(document).on('click', function(event){
    if( $(event.target).closest('.filter-hidd').length)
        return;
    $('.filter--box').removeClass('active');
    $('.bgFiler').removeClass('hover');
    event.stopPropagation();
});
//Маска телефона
$.masked = function () {
    var $el = $('.phone-masked');
    $el.mask("+7(999)999-99-99", {autoclear: false});
}

//validator
$.Valid = function (el) {

    var $el = el;

    if (!$el.length) return;
    var validator = $el.validate({
        rules: {
            name: {
                required: true
            },
            surname: {
                required: true
            },
            password: {
                required: true
            },
            password1: {
                required: true,
                minlength: 6,
                pwcheck: true
            },
            password2: {
                required: true,
                minlength: 6,
                equalTo: '[name="password1"]'
            },
            tel: {
                required: true,
                checkMask: true
            },
            telMail: {
                required: true,
                checkTelMail: true
            },
            check: {
                required: true
            },
            size: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            submitHandler: function (form) {
//                form.submit();
            }
        }
    })
    $.validator.addMethod("pwcheck", function(value) {
       return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) 
           && /[a-z]/.test(value) 
           && /\d/.test(value) 
    }, "Incorrect Password!");
     $.validator.addMethod("checkMask", function(value) {
         return /\+\d{1}\(\d{3}\)\d{3}-\d{2}-\d{2}/g.test(value); 
    });
     $.validator.addMethod("checkTelMail", function(value) {
         return /\+[0-9]{1,4}[0-9]{1,10}|(.*)@(.*)\.[a-z]{2,5}/g.test(value); 
    });
};
 
//menu
$(document).on('click', '.btMenu', function(){
    $(this).toggleClass('is-active');
    $('.bg').toggleClass('is-active');
    $('body').toggleClass('ov-hidd');
    $('html').toggleClass('ov-hidd');
    return false
});
$(window).on('load', function(){
    $('.menu').css({
        "opacity" : "1"
    });
    $('.basketBox').css({
        "opacity" : "1"
    });
    $('.oneClickBox').css({
        "opacity" : "1"
    });
});
//Basket 
$(document).on('click', '.btBasket', function(){
    $(this).toggleClass('is-active');
    $('.bg').toggleClass('is-active');
    $('body').toggleClass('ov-hidd');
    $('html').toggleClass('ov-hidd');
    return false
})
$(document).on('click', '.js-click', function(){
    $(this).closest('.btBasket').removeClass('is-active');
    $('.oneClickBox').addClass('is-active');
    return false
});
$(document).on('click', '.js-basket', function(){
    $(this).closest('.btBasket').addClass('is-active');
    $('.oneClickBox').removeClass('is-active');
    return false
});
$(document).on('click', '.js-close', function(){
    $('.btBasket').removeClass('is-active');
    $('.oneClickBox').removeClass('is-active');
    $('.btMenu').removeClass('is-active');
    $('.bg').removeClass('is-active');
    $('.menuCity').removeClass('is-open');   
    $('body').removeClass('ov-hidd');
    $('html').removeClass('ov-hidd');
    return false
});
$(document).on('click', '.bg', function(){
    $('.btBasket').removeClass('is-active');
    $('.oneClickBox').removeClass('is-active');
    $('.btMenu').removeClass('is-active');
    $('.bg').removeClass('is-active');
    $('.menuCity').removeClass('is-open');
    $('.mmFilter').removeClass('is-active');
    $('body').removeClass('ov-hidd');
    $('html').removeClass('ov-hidd');
});
$(document).on('click', '.js-openCity', function(){
    $('.menuCity').toggleClass('is-open');
    $.menuCityTop();
    return false
});
$(document).on('click', '.tableBasket tr:not(.remove) .rem', function(){
    $(this).closest('tr').addClass('remove').find('td').last().prepend('<span class="removeText">товар удален</span>');
    return false
});
$(document).on('click', '.tableBasket tr.remove .rem', function(){
    $(this).closest('tr').removeClass('remove').find('.removeText').remove();
    return false
});
$.menuCityTop = function(){
    var cityTop = $('.menu .city').offset().top;        
    $('.menuCity .citySearch').css('margin-top', cityTop + 'px')
};
$.menuCityTop();
$(window).on('resize', function(){
    $.menuCityTop();
})
//Search
$.searc = function(){
    var topbBg = $('.hhead').outerHeight();
    $('.main-block').append($('<div class="bgSearch"/>'))
//    $('.search .bgSearch').css('top', topbBg + 'px')
    $(document).on('click', '.bt_search_open', function(event){
        $(this).next('.hidd').fadeToggle();
        $('.search .input-search').focus();
        $('.search').addClass('open');
        $('.bgSearch').addClass('active');
        event.stopPropagation(); 

    });
    $(document).on('keypress', '.search .input-search', function (){
        if ($(this).val()!='') $(this).closest('.search').addClass('active');
    });
    $(document).on('click', function(event){
        $('.search').removeClass('open');
        if( $(event.target).closest('.search').length) 
            return; 
            $('.search .hidd').fadeOut().closest('.search').removeClass('active');
            $('.bgSearch').removeClass('active');

        event.stopPropagation();

    });
    $(document).on('click', '.js-close-search', function(event){
        $(this).closest('.hidd').fadeToggle();
        $('.search').removeClass('open').removeClass('active'); 
        $('.bgSearch').removeClass('active');
        event.stopPropagation();
    });
}();
$.newsSlider = function(){
    var slidesPerView_var = 1;
    if($('.page__news--slider-container .swiper-slide').length == 5) {
        slidesPerView_var = 5;
    } else if($('.page__news--slider-container .swiper-slide').length == 4){
        slidesPerView_var = 4;
    } else if($('.page__news--slider-container .swiper-slide').length == 3){
        slidesPerView_var = 3;
    } else if($('.page__news--slider-container .swiper-slide').length == 2){
        slidesPerView_var = 2;
    } else {
        //slidesPerView_var = 5;
    }

    $('.page__news--slider-container').each(function(index, value){
        var swiper = new Swiper(value, {
            loop: $('.page__news--slider-container .swiper-slide').length > 5 ? true : false,
            watchOverflow: true,
            grabCursor: true,
            slidesPerView: slidesPerView_var,
            spaceBetween: 40,
            //loop: true,
            loopedSlides: 3,
            speed: 1000,
            parallax: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: '.page__news-swiper-button-next',
                prevEl: '.page__news-swiper-button-prev',
            },
            breakpoints: {
                0: {
                  slidesPerView: 1,
                  spaceBetween: 20
                },
                460: {
                  slidesPerView: 2,
                  spaceBetween: 20
                },
                767: {
                  slidesPerView: 3,
                  spaceBetween: 30
                },
                1000: {
                  slidesPerView: 4,
                  spaceBetween: 38
                }
            }
        });
        
    });
};
$.catalogSlider = function(){
    $('.catalog--slider').each(function(index, value){
        var numberOfSlides = $(this).find('.swiper-slide').length;
        var swiper = new Swiper(value, {
            slidesPerView: 5,
            loop: numberOfSlides <= 5 ? false : true,
            speed: 1000,
            parallax: true,
            loopAdditionalSlides: 10,
            navigation: {
                nextEl: value.nextElementSibling.nextElementSibling,
                prevEl: value.nextElementSibling,
            },
            breakpoints: {
                0: {
                    slidesPerView: 2,
                    loop: numberOfSlides <= 2 ? false : true,
                    spaceBetween: 20
                },
                480: {
                    slidesPerView: 3,
                    loop: numberOfSlides <= 3 ? false : true,
                    spaceBetween: 30
                },
                670: {
                    slidesPerView: 3,
                    loop: numberOfSlides <= 3 ? false : true,
                    spaceBetween: 30
                },
                1000: {
                    slidesPerView: 4,
                    loop: numberOfSlides <= 4 ? false : true,
                    spaceBetween: 30
                },
                1440: {
                    slidesPerView: 6,
                    loop: numberOfSlides <= 6 ? false : true,
                    spaceBetween: 40
                },
                1920: {
                    slidesPerView: 9,
                    loop: numberOfSlides <= 9 ? false : true,
                    spaceBetween: 40
                }
            }
        });
        
    });
};
$.catalogSliderBig = function(){
    $('.catalog--slider-big').each(function(index, value){
        if ($(window).width() >= 1920) {
            if ($(value).find($('.swiper-slide')).length > 7) {
                var swiper = new Swiper(value, {
                    slidesPerView: 6,
                    spaceBetween: 40,
                    loop: true,
                    loopedSlides: 5,
                    speed: 1000,
                    parallax: true,
                    loopFillGroupWithBlank: true,
                    navigation: {
                        nextEl: value.nextElementSibling.nextElementSibling,
                        prevEl: value.nextElementSibling
                    },
                    breakpoints: {
                        0: {
                            slidesPerView: 2,
                            spaceBetween: 20
                        },
                        480: {
                            slidesPerView: 3,
                            spaceBetween: 30
                        },
                        670: {
                            slidesPerView: 3,
                            spaceBetween: 10
                        },
                        1000: {
                            slidesPerView: 4,
                            spaceBetween: 40
                        },
                        1920: {
                            slidesPerView: 7,
                            spaceBetween: 40
                        }
                    }
                });
            } else {
                $(value).closest('.catalog--slider--wrapper').addClass('swiper-button--hide');
            }
        } else if ($(window).width() < 1920) {
            if ($(value).find($('.swiper-slide')).length > 4) {
                var swiper = new Swiper(value, {
                    slidesPerView: 6,
                    spaceBetween: 40,
                    loop: true,
                    loopedSlides: 5,
                    speed: 1000,
                    parallax: true,
                    loopFillGroupWithBlank: true,
                    navigation: {
                        nextEl: value.nextElementSibling.nextElementSibling,
                        prevEl: value.nextElementSibling
                    },
                    breakpoints: {
                        0: {
                            slidesPerView: 2,
                            spaceBetween: 20
                        },
                        480: {
                            slidesPerView: 3,
                            spaceBetween: 30
                        },
                        670: {
                            slidesPerView: 3,
                            spaceBetween: 10
                        },
                        1000: {
                            slidesPerView: 4,
                            spaceBetween: 40
                        },
                        1920: {
                            slidesPerView: 7,
                            spaceBetween: 40
                        }
                    }
                });
            } else {
                $(value).closest('.catalog--slider--wrapper').addClass('swiper-button--hide');
            }
        }
    });
};
$.mainSlider = function() {
    // Params
let mainSliderSelector = '.main-slider',
    interleaveOffset = 0.5;

    // Main Slider
    let mainSliderOptions = {
        loop: $('.main-slider .swiper-slide').length > 1 ? true : false,
        speed: 1000,
        autoplay: {
            delay: 3000
        },
        loopAdditionalSlides: 10,
        watchOverflow: true,
        grabCursor: true,
        watchSlidesProgress: true,
        pagination: {
            el: '.swiper-pagination',
          },
        /*autoHeight: true,*/
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        on: {
            init: function () {
                this.autoplay.stop();
            },
            imagesReady: function () {
                this.el.classList.remove('loading');
                this.autoplay.start();
            },
            slideChangeTransitionEnd: function () {
                let swiper = this,
                    captions = swiper.el.querySelectorAll('.caption');
                for (let i = 0; i < captions.length; ++i) {
                    captions[i].classList.remove('show');
                }
                swiper.slides[swiper.activeIndex].querySelector('.caption').classList.add('show');
            },
            progress: function () {
                let swiper = this;
                for (let i = 0; i < swiper.slides.length; i++) {
                    let slideProgress = swiper.slides[i].progress,
                        innerOffset = swiper.width * interleaveOffset,
                        innerTranslate = slideProgress * innerOffset;

                    swiper.slides[i].querySelector(".slide-bgimg").style.transform =
                        "translateX(" + innerTranslate + "px)";
                }
            },
            touchStart: function () {
                let swiper = this;
                for (let i = 0; i < swiper.slides.length; i++) {
                    swiper.slides[i].style.transition = "";
                }
            },
            setTransition: function (speed) {
                let swiper = this;
                for (let i = 0; i < swiper.slides.length; i++) {
                    swiper.slides[i].style.transition = speed + "ms";
                    swiper.slides[i].querySelector(".slide-bgimg").style.transition =
                        speed + "ms";
                }
            }
        }
    };
    let mainSlider = new Swiper(mainSliderSelector, mainSliderOptions);
}

$('[data-img]').each(function(){
    $(this).addClass('ainimated').prepend('<i class="bg-img"/>');
    var imgLink = $(this).data('img');
    $(this).find('.bg-img').css('background-image', 'url('+ imgLink +')');
});
$('.ainimated').parents('ul:not(.catalog)').each(function(){
    if($(this).find('li').length > 2){
        if(!$(this).hasClass('delay')){
            $(this).addClass('delay');
            $('li', this).each(function(i){
                    $(this).find('.bg-img').css('transition-delay' , (i/10*2) + 's');
              
              })
        }
    }      
});
var wow = new WOW(
  {
    boxClass:     'ainimated',      
    animateClass: 'on', 
    offset:       0,          
    mobile:       true,     
    live:         true,
    scrollContainer: null 
  }
);
wow.init();

$.instaWidth = function(){
    var $this = $('.insta--list'),
        width = $(window).width(),
        widthEl = $this.find('li').innerWidth(),
        sumVisible11 = width / widthEl,
        sumVisible = Math.floor(width / widthEl); 
    $this.find('li').removeClass('hidd')
    $this.find('li').slice(sumVisible).addClass('hidd');
};
$.instaWidth()
$(window).on('resize', function(){
    $.instaWidth()
});
//filter 
if($('.filter').length){
    $('.filter-title').append('<span class="sel"/>');
}
// $(document).on('click', '.filter .rem', function(){
//     $(this).closest('.filter--box').find('[type="checkbox"]', '[type="radio"]').prop('checked', false);
//     $(this).closest('.filter--box').find('.filter-title').removeClass('visible').removeClass('visibleSize');
// });

$(document).on('click', '.filter--ok', function(){
    var chek = $(this).closest('.filter-hidd').find('input:checked');
    $(this).closest('.filter--box').find('.sel').empty();
    chek.siblings('span').clone().addClass('btCheck').prependTo($(this).closest('.filter--box').find('.sel'));
    if(chek.length ){
        if(chek.length > 1){
            $(this).closest('.filter--box').find('.filter-title').addClass('visible');
            $(this).closest('.filter--box').find('.filter-title').removeClass('visibleOne');
        }else {
            $(this).closest('.filter--box').find('.filter-title').removeClass('visible');
            $(this).closest('.filter--box').find('.filter-title').addClass('visibleOne');
        }
    } else {
        $(this).closest('.filter--box').find('.filter-title').removeClass('visible').removeClass('visibleOne');
    }
});

$(document).on('click', '.filter--ok-ui', function(){
    var priceFilterMin = $(this).closest('.filter-hidd').find('.minCost').val(),
        priceFilterMax = $(this).closest('.filter-hidd').find('.maxCost').val();
    
    $(this).closest('.filter-price').find('.sel').html(priceFilterMin + '-' + priceFilterMax);
    $(this).closest('.filter-price').find('.filter-title').addClass('visible');
});    
if($('.goods').length){
    //$('.goods--info .description').prepend($('.goods--info .desc').clone());
    $('.goods--info-select .title-gray').append('<span class="inp"/>')
    $(document).on('change', '.goods--info-select .checkbox', function(){
        var chek = $(this).closest('div').find('input:checked');
        $(this).closest('div').find('.inp').empty();
        chek.siblings('span').clone().addClass('btCheck').prependTo($(this).closest('div').find('.inp'));

        if(chek.length ){
            $(this).closest('div').find('.title-gray').addClass('visible');
        } else {
            $(this).closest('div').find('.title-gray').removeClass('visible');
        }

        $(this).closest('div').find('.title-gray').removeClass('is-active');
    });
}
$(document).on('touchstart', '.goods--info-select .title-gray', function(){
    $(this).toggleClass('is-active');
})
$.goodsSlider = function(){

        var galleryThumbs = new Swiper('.goods--slider .goods--slider--swiper--nav', {
            direction: 'vertical',
            navigation: {
                nextEl: '.goods--slider .goods-next',
                prevEl: '.goods--slider .goods-prev',
            },
            spaceBetween: 15,
            centeredSlides: false,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true,
            loop: false,
            autoHeight: true,
        })


    var galleryTop = new Swiper('.goods--slider .goods--slider--swiper', {
        loop: false,
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
        },
        thumbs: {
            swiper: galleryThumbs,
          },
    });
};

$.zoomBox = function(){
    $('.zoomBox').each(function(index, value){
       // $(this).mlens({
       //     imgSrc: $(this).attr("data-big"),	  // path of the hi-res version of the image
       //     imgSrc2x: $(this).attr("data-big2x"),  // path of the hi-res @2x version of the image
       //     lensShape: "square",
       //     lensSize: ["20%","30%"],
       //     borderSize: 0,
       //     borderRadius: 0,
       //     zoomLevel: 1,
       //     responsive: true,
       //     overlayAdapt: false
       // });
       // $(this).zoom({url: $(this).attr("data-src")});

        var $this = $(this);
        $this
            .wrap('<span class="wrp" style="display:inline-block"></span>')
            .css({"display":"block", "cursor":"crosshair"});
        $this.closest('.swiper-slide').zoom();
    });
}
$(document).on('click', '.js-openBt', function(event){
    $(this).toggleClass('is-active').siblings('.hidd').slideToggle();
    return false
});
//popup
$(document).on('click', '[data-fancyajax]:not(.linkPage)', function(){
    var src = $(this).attr('href'),
        bt = $(this);
    $.fancybox.open({
        src: src,
        type: 'inline',
        opts: {
            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small" title="{{CLOSE}}">' +
                    '<svg class="bt-close">' +
                        '<use xlink:href="#close"></use>' +
                    '</svg>' +
                '</button>'
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            afterShow: function() {
                $('.js-nospace').each(function(){
                    $.nospace();
                });
                $('.Valid').each(function(){
                    $.Valid($(this));
                });   
                $('select').each(function(){
                    $('select').select2({
                        width: 'none',
                        searchInputPlaceholder: 'Введите название города',
                        language: {
                           "noResults": function(){
                               return "Ничего не найдено";
                           }
                       },
                    });
                });   
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                    
                
            },
            afterLoad: function(instance, slide) {
            }
        }
    });    
});
$('select:not(.selectMaps)').each(function(){
    $('select:not(.selectMaps)').select2({
        width: 'none'
    });
});   
$('select.selectMaps').each(function(){
    $('select.selectMaps').select2({
        width: 'none',
        minimumResultsForSearch: Infinity
    });
});   

//tabs
$('.tabsContainerDevelory ul.tabs').on('click', 'li:not(.current)', function() {
    $(this).addClass('current').siblings().removeClass('current')
    .parents('.tabsContainerDevelory').find('.tabsBox').eq($(this).index()).fadeIn().siblings('.tabsBox').hide();
    var id = $(this).attr("rel");
    $("#ID_DELIVERY_ID_"+id).click();
    return false
});

$(document).on('click', '.selectBoxCheck-open', function(){
    $(this).closest('.selectBoxCheck').toggleClass('open');
    return false
});

$(document).on('change', '.checkboxImg input', function(){
    var nameShop = $(this).closest('.checkboxImg').find('.name').text();
    $(this).closest('.selectBoxCheck').find('.selectBoxCheck-open').html(nameShop);
    $('.selectBoxCheck').removeClass('open');
});
$(document).on('click', '.js-question', function(){
    $(this).closest('.page__refund__question__box').siblings('li').find('.page__refund__question__hidd').removeClass('open').slideUp();
    $(this).siblings('.page__refund__question__hidd:not(.open)').slideDown().find('.close').addClass('open');
    return false
});
$(document).on('click', '.page__refund__question__hidd .close', function(){
    $(this).closest('.page__refund__question__hidd').slideUp().find('.close').removeClass('open');
    return false
});
if($('.castomWidth-list').length){
    $('html').addClass('castomWidthSelect');
}
$.fancyImgOptions = ({}, $.fancyOptions, {
    buttons: [
        "close"
    ],
    thumbs: {
        autoStart: false,
    },
    afterShow: function() {
        $('.Valid').each(function(){
            $.Valid($(this));
        });
    },
    btnTpl: {
       close:'<button data-fancybox-close class="fancybox-close-small" title="{{CLOSE}}" data-png="images/icons-close.png">' +
                    '<svg style="width: 1.125rem; height: 1.125rem; fill: #fff;">' +
                        '<use xlink:href="#close"></use>' +
                    '</svg>' +
                '</button>',
        arrowLeft:
          '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' +
              '<div>' + 
                 '<svg style="transform: rotate(180deg); width: 1.63rem; height: 1.188rem; fill: #fff;">' +
                    '<use xlink:href="#arrow-right-left"></use>' +
                 '</svg>' +
              '</div>' +
          '</button>',

        arrowRight:
          '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' +
              '<div>' + 
                 '<svg style="width: 1.63rem; height: 1.188rem; fill: #fff;">' +
                    '<use xlink:href="#arrow-right-left"></use>' +
                 '</svg>' +
              '</div>' +
          '</button>'
    },
    lang: "ru",
    i18n: {
        ru: {
          CLOSE: "Закрыть",
          NEXT: "Вперёд",
          PREV: "Назад",
          ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
          PLAY_START: "Запустить слайдшоу",
          PLAY_STOP: "Остановить слайдшоу",
          FULL_SCREEN: "На весь экран",
          THUMBS: "Миниатюры",
          DOWNLOAD: "Скачать",
          SHARE: "Поделиться",
          ZOOM: "Увеличить"
        }
    }
});
$.fancyImages = function (el) {

    var $el = el;

    if (!$el.length) return;


    $el.fancybox($.fancyImgOptions);

};  
$('.orderPage__form .item').last().after('<div class="order-composition"><a href="#" class="link">Состав заказа<a/><div class="hidd"/></div>');
$('.order-composition .hidd').append($('.orderPage__tableBasket').clone().removeClass('ainimated'));
$(document).on('click', '.order-composition .link', function(){
    $(this).closest('.order-composition').toggleClass('open');
    return false
});
$('.menuCatalog .firstLvl').hover(function(){
        var $this = $(this);
        $this.addClass('hover')
        setTimeout(function() {
            $this.siblings('.firstLvl').removeClass('hover');
        }, 150);
    },
    function(){
        var $this = $(this);
        setTimeout(function() {
            $this.removeClass('hover')
        }, 200);
});
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('html').addClass('scroll');
    } else {
        $('html').removeClass('scroll');
    }
});

$(window).resize(function () {
    if ($(window).width() > 1000) {
        if ($('.size_error').length) {
            $('.size_error').appendTo('.cart_block');
        }
    } else {
        if ($('.size_error').length) {
            $('.size_error').appendTo('.goods--info-select');
        }
    }
});
(function($) {

    //удалить с корзины
    $(document).on('click', '.tableBasket tr:not(.remove) .b-rem', function(){
        $(".sum_base").load("/inc/ajax/sum_base.php",{"del":$(this).attr('data-rel')});
        $('#'+$(this).attr('data-rel')).attr("name"," ");
        b_loaded();
        return false
    });
    //восстановить товар в корзине
    $(document).on('click', '.tableBasket tr.remove .b-rem', function(){
        $(".sum_base").load("/inc/ajax/sum_base.php",{"add":$(this).attr('rel'),'num':1});
        $('#'+$(this).attr('data-rel')).attr("name",$(this).attr('rel'));
        b_loaded();
        return false
    });
    //удалить с корзины в оформлении
    $(document).on('click', '.tableBasket tr:not(.remove) .rems', function(){
        $(".sum_base").load("/inc/ajax/sum_base.php",{"del":$(this).attr('data-rel')});
        $('#'+$(this).attr('data-rel')).attr("name"," ");
        //b_loaded();
        submitForm();
        return false
    });
    //восстановить товар в корзине в оформлении
    $(document).on('click', '.tableBasket tr.remove .rems', function(){
        $(".sum_base").load("/inc/ajax/sum_base.php",{"add":$(this).attr('rel'),'num':1});
        $('#'+$(this).attr('data-rel')).attr("name",$(this).attr('rel'));
        //b_loaded();
        submitForm();
        return false
    });
        //передаем данные в фильтр
        $('.filter--box').each(function(){
            var chek = $(this).find('input:checked');
            chek.parents('.filter--box').find('.sel').empty();
            chek.siblings('span').clone().addClass('btCheck').prependTo(chek.parents('.filter--box').find('.sel'));

            if(chek.length ){
                if(chek.length > 1){
                   chek.parents('.filter--box').find('.filter-title').addClass('visible');
                    chek.parents('.filter--box').find('.filter-title').removeClass('visibleOne');
                   chek.parents('.filter--box').find('.filter-titles').addClass('visible');
                    chek.parents('.filter--box').find('.filter-titles').removeClass('visibleOne')                   
                }else {
                     chek.parents('.filter--box').find('.filter-title').removeClass('visible');
                     chek.parents('.filter--box').find('.filter-title').addClass('visibleOne');
                     chek.parents('.filter--box').find('.filter-titles').removeClass('visible');
                     chek.parents('.filter--box').find('.filter-titles').addClass('visibleOne');
                }
            } else {
               chek.parents('.filter--box').find('.filter-title').removeClass('visible').removeClass('visibleOne');
            }  

            if($(".maxCost").val() && $(".minCost").val()){
                if($(".minCost").val()!=$(".minCost").attr("placeholder") || $(".maxCost").val()!=$(".maxCost").attr("placeholder")){
                    $('.filter-price').find('.sel').html($(".minCost").val() + ' руб. -' + $(".maxCost").val()+' руб.');
                    $('.filter-price').find('.filter-title').addClass('visible');
                }
            }
        });

}(jQuery));
