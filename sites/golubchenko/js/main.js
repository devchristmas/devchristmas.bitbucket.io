// Главный слайдер
let headerSlider = $('#header-slider')
headerSlider.owlCarousel({
  nav: true,
  animateIn: 'fadeIn',
  animateOut: 'fadeOut',
  autoHeight: false,
  items: 1,
  mouseDrag: false,
  // autoplay: true,
  // autoplayTimeout: 5000,
  dots:false,
  loop: true,
  navText: [
    `<svg width="41" height="8" viewBox="0 0 41 8" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0.646446 3.64645C0.451183 3.84171 0.451183 4.15829 0.646446 4.35355L3.82843 7.53553C4.02369 7.7308 4.34027 7.7308 4.53553 7.53553C4.7308 7.34027 4.7308 7.02369 4.53553 6.82843L1.70711 4L4.53553 1.17157C4.7308 0.976311 4.7308 0.659728 4.53553 0.464466C4.34027 0.269204 4.02369 0.269204 3.82843 0.464466L0.646446 3.64645ZM41 3.5H1V4.5H41V3.5Z" fill="#413239"/>
    </svg>`,
    `<svg width="41" height="8" viewBox="0 0 41 8" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M40.3536 4.35355C40.5488 4.15829 40.5488 3.84171 40.3536 3.64645L37.1716 0.464466C36.9763 0.269204 36.6597 0.269204 36.4645 0.464466C36.2692 0.659728 36.2692 0.976311 36.4645 1.17157L39.2929 4L36.4645 6.82843C36.2692 7.02369 36.2692 7.34027 36.4645 7.53553C36.6597 7.7308 36.9763 7.7308 37.1716 7.53553L40.3536 4.35355ZM0 4.5H40V3.5H0V4.5Z" fill="#413239"/>
    </svg>
    `
  ],
  onInitialized: (data) => {
    $('.header-slider__count .count').text(data.item.count);
    $('.header-slider__count .current').text(1);
  },
  onChanged: (data) => {
    $('.header-slider__count .current').text(data.item.index - 1);
  }
});

// UI Form and Masks
$('[data-ui-date]').datetimepicker({timepicker:false});
$('[data-ui-time]').datetimepicker({datepicker:false,format:'H:i'});
$('[data-ui-phone]').mask('+7(999) 999 99 99')



// Модалки
$('[data-show-modal]').on('click', (e) => {
  e.preventDefault();
  let modalId = $(e.target).attr('data-show-modal');
  if ($(modalId)) {
    $('html, body').addClass('js-noscrolling');
    $('.ui-modal').fadeOut();
    $(modalId).fadeIn().css('display', 'flex');
  }
});
$('.ui-modal__close').on('click', (e) => {
  e.preventDefault();
  $('.ui-modal').fadeOut();
  $('html, body').removeClass('js-noscrolling');
})
$('.ui-modal').on('click', (e) => {
  if ($(e.target).is('.ui-modal')) {
    $('.ui-modal').fadeOut();
    $('html, body').removeClass('js-noscrolling');
  }
});





// Scroll
SmoothScroll({ stepSize: 55, animationTime: 600 });
$("[data-custom-scroll]").mCustomScrollbar();



// Аккордион в секции "Эффективно лечим"
$('.section-effective-list__caption').on('click', (e) => {
  let item = $(e.target).closest('.section-effective-list__item');
  let text = $(item).find('.section-effective-list__text');
  
  $('.section-effective-list__item').removeClass('js-active');
  $('.section-effective-list__text').stop().slideUp();
  
  $(item).addClass('js-active');
  $(text).stop().slideDown();
});
$('.section-effective-list__item:eq(0) .section-effective-list__caption').click();


// Слайдер (Методы лечения)
let slideAnimataion = new TimelineMax();
$('.section-methods-list li').on('click', (e) => {
  e.preventDefault();
  $('.section-methods-list li').removeClass('js-active');
  $(e.target).addClass('js-active');
  let index = $(e.target).index();

  if ($(`.section-methods__content:eq(${index})`)) {
    $('.section-methods__content').stop().fadeOut(300).removeClass('js-active');
    $(`.section-methods__content:eq(${index})`)
    .addClass('js-active')
    .stop()
    .fadeIn(500)
    .css('display', 'flex')
  }
  
});
$('.section-methods-list li:first-child').click();



// Отзывы
$('.section-reviews-list__item-button a').on('click', (e) => {
  e.preventDefault();
  let item = $(e.target).closest('.section-reviews-list__item');
  let moreText = $(item).find('.section-reviews-list__item-more');
  
  $('.section-reviews-list__item-more').stop().slideUp();
  $('.section-reviews-list__item-button a').show();

  $(e.target).hide();
  moreText.stop().slideDown();
});