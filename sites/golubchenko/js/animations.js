// Scroll Magic Scene Controller

if ($(window).width() >= 1280) {
    
  let controller = new ScrollMagic.Controller();
  //////////////////////////////////////////////

  // Header
  ////////////////////////////////////////////////////////////

  // Section 1
  let section1 = new TimelineMax();
  section1.add([
    TweenLite.fromTo('#section-1 .section__title .title-stroke', 1.5, {x:-200, opacity: 0}, {x:0, opacity: 1}),
    TweenLite.fromTo('#section-1 .section-1__title .text-brown', 3, {x:200, opacity: 0}, {x:0, opacity: 1}),
    TweenLite.fromTo('#section-1 .section__content .section__title', 2, {opacity:0}, {opacity:1}),
    TweenLite.fromTo('#section-1 .section-1__image img', 1, {y:100}, {y:0})
  ]);
  let sceneSection1 = new ScrollMagic.Scene({
    triggerElement: '#section-1',
    triggerHook: 0.4,
    duration: '700px'
  }).setTween(section1).addTo(controller)
  ///////////////////////////////////////////////////////////


  // Section About
  let sectionAbout = new TimelineMax();
  sectionAbout
  .add([
    TweenLite.fromTo('.section-about__time', 3, {y:-110}, {y:150}, 0),
    TweenLite.fromTo('.section-about__image img', 3, {y:-100}, {y:0}, 0)
  ])
  .add([
    TweenLite.fromTo('.section-about__title .section__title', 1, {x:-200, opacity: 0}, {x:0, opacity: 1}),
    TweenLite.fromTo('.section-about__title .section__text', 3, {x:-500, opacity: 0}, {x:0, opacity: 1}),
    TweenLite.fromTo('.section-about__image-text', 4, {scale:0}, {scale:1})
  ]);
  let sceneSectionAbout = new ScrollMagic.Scene({
    triggerElement: '#section-about',
    duration: '1200px'
  }).setTween(sectionAbout).addTo(controller)
  ///////////////////////////////////////////////////////////


  // Section Directions
  let sectionDirections = new TimelineMax();
  sectionDirections.add([
    TweenLite.fromTo('.section-directions__title .num', 1, {y:-150}, {y: 0}, '0'),
    TweenLite.fromTo('.section-directions__content', 1, {y:200}, {y: 0}, '0'),
    TweenLite.fromTo('.section-directions__item', 1, {opacity: 0}, {opacity: 1}, '0')
  ]);
  let sceneSectionDirections = new ScrollMagic.Scene({
    triggerElement: '.section-directions',
    duration: '600px'
  }).setTween(sectionDirections).addTo(controller);
  ///////////////////////////////////////////////////////////


  // Section Education
  let sectionEducation = new TimelineMax();
  sectionEducation.add([
    TweenLite.fromTo('.section-education__years', 2, {x:-200, opacity: 0}, {x: 0, opacity: 1}, '0'),
    TweenLite.fromTo('.section-education__years + *', 2, {x:200, opacity: 0}, {x: 0, opacity: 1}, '0'),
    TweenLite.fromTo('.section-education__title', 2, {x:-500}, {x: 0}, '0')
  ]);
  let sceneSectionEducation = new ScrollMagic.Scene({
    triggerElement: '.section-education',
    duration: '600px',
    triggerHook: 0.7
  }).setTween(sectionEducation).addTo(controller);
  ///////////////////////////////////////////////////////////


  // Section Effective
  let sectionEffective = new TimelineMax();
  sectionEffective.add([
    TweenLite.fromTo('.section-effective__image img', 5, {y:150}, {y: 0}, 0),
    TweenLite.fromTo('.section-effective__title', 5, {x:200, opacity: 0}, {x: 0, opacity: 1}, 0),
    TweenLite.fromTo('.section-effective__title .title-stroke', 5, {x:400}, {x: 0}, 0)
  ]);
  let sceneSectionEffective = new ScrollMagic.Scene({
    triggerElement: '.section-effective',
    duration: '600px'
  }).setTween(sectionEffective).addTo(controller);
  ///////////////////////////////////////////////////////////




  // Section Diagnostic
  let sectionDisagnostic = new TimelineMax();
  sectionDisagnostic

  .add([
    TweenLite.fromTo('.section-diagnostic__title', 2, {x: -200, opacity: 0}, {x: 0, opacity: 1}),
    TweenLite.fromTo('.section-diagnostic .section__title .title-stroke', 4, {x: -300, opacity: 0}, {x: 0, opacity: 1}),
    TweenLite.fromTo('.section-diagnostic__subtitle', 6, {x: 400, opacity: 0}, {x: 0, opacity: 1}),
    TweenLite.fromTo('.section-diagnostic__text', 7, {x: 100, opacity: 0}, {x: 0, opacity: 1})
  ])
  let sectionDisagnostic2 = new TimelineMax();
  sectionDisagnostic2.add([
    TweenLite.fromTo('.section-diagnostic__row2 .section__subtitle', 2, {x: -200, opacity: 0}, {x: 0, opacity: 1}, '0'),
    TweenLite.fromTo('.section-diagnostic__row2 .section__subtitle + *', 3, {x: -400, opacity: 0}, {x: 0, opacity: 1}, '0'),
    TweenLite.fromTo('.section-diagnostic__image img', 5, {y: -100, opacity: 0}, {y: 0, opacity: 1}, '0')
  ]);
  let sceneSectionDiagnostic = new ScrollMagic.Scene({
    triggerElement: '.section-diagnostic',
    triggerHook: .7,
    duration: '700px'
  }).setTween(sectionDisagnostic).addTo(controller);
  // Section Diagnostic 2
  let sceneSectionDiagnostic2 = new ScrollMagic.Scene({
    triggerElement: '.section-diagnostic__row2',
    triggerHook: 0.8,
    duration: '400px'
  }).setTween(sectionDisagnostic2).addTo(controller);
  ///////////////////////////////////////////////////////////




  // Section Methods
  let sectionMethods = new TimelineMax();
  sectionMethods.add([
    TweenLite.fromTo('.section-methods .section__title', 2, {x: -200, opacity: 0}, {x: 0, opacity: 1}),
    TweenLite.fromTo('.section-methods .section__subtitle', 2, {x: 300, opacity: 0}, {x: 0, opacity: 1}),
    TweenLite.fromTo('.section-methods .section__title .title-stroke', 4, {x: -200, opacity: 0}, {x: 0, opacity: 1})
  ]).add([
    TweenLite.fromTo('.section-methods__content-title', 3, {x: -30}, {x: 0}),
    TweenLite.fromTo('.section-methods__image img', 3, {y: -50}, {y: 0})
  ])
  let sceneSectionMethods = new ScrollMagic.Scene({
    triggerElement: '.section-methods',
    triggerHook: 0.8,
    duration: '1600px'
  }).setTween(sectionMethods).addTo(controller);
  ///////////////////////////////////////////////////////////

  // Section Methods
  let sectionMethods2 = new TimelineMax();
  sectionMethods2.add(TweenLite.fromTo('.section-methods-ready__item:nth-child(1)', 1, {x:-100, opacity: 0}, {x:0, opacity: 1}))
  .add(TweenLite.fromTo('.section-methods-ready__item:nth-child(2)', 2, {x:100, opacity: 0}, {x:0, opacity: 1}))
  .add(TweenLite.fromTo('.section-methods-ready__item:nth-child(3)', 3, {x:-100, opacity: 0}, {x:0, opacity: 1}))
  .add(TweenLite.fromTo('.section-methods-ready__item:nth-child(4)', 4, {x:100, opacity: 0}, {x:0, opacity: 1}))

  let sceneSectionMethods2 = new ScrollMagic.Scene({
    triggerElement: '.section-methods-ready__list',
    triggerHook: 0.7,
    duration: '600px'
  }).setTween(sectionMethods2).addTo(controller);


  // Section Reviews
  new ScrollMagic.Scene({
    triggerElement: '.section-reviews',
    triggerHook: 0,
    duration: $('.section-reviews').height() - 20
  })
  .setPin('#section-reviews-title', {pushFollowers: false})
  .addTo(controller);
  ///////////////////////////////////////////////////////////

}