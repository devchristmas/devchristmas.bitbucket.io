let sections_pos = [];

gsap.registerPlugin(ScrollTrigger);

function $d (maxWidth = 0){
    return window.innerWidth > maxWidth;
}

$(window).on('load', () => {



    !(function () {

        $('.photo-slider').owlCarousel({
            items: 1,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut'
        });

        $('.photo-slider').on('changed.owl.carousel', (e) => {
            const counter = $(e.currentTarget).parents('.slider-grid').find('.controls .slider-counter');
            const itemCurrent = e.item.index;
            const itemsCount = e.item.count;

            counter.find('.current').text(itemCurrent + 1);
            counter.find('.all').text(itemsCount);
        })


        !(function () {



            if (window.innerWidth > 10000000) {


                //626 + 4112 + 2377 + 2932 + 2800 + 1460
                const sections = $('.wrapper > section');

                let curretPage = $('section.current-page');
                let scrollTo = 0;
                let scrollDif = 0;
                let delta = 0;

                let totalHeight = 0;

                function scrolling(e) {
                    $(window).resize();
                    $('html, body').resize();

                    const scrollTop = $(window).scrollTop();

                    if (scrollTop > scrollTo) { delta = +1; } else {delta = -1;}
                    scrollTo = scrollTop;

                    sections_pos.forEach((s, i) => {
                        if (scrollTop > (s.top + 20)) {

                            if (scrollTop > (s.top + s.height)) {
                                curretPage = $(s.el).next();
                                scrollDif = sections_pos[i + 1].top;
                            } else {
                                curretPage = s.el;
                                scrollDif = s.top;
                            }

                        }
                    })



                    const tl = new TimelineMax();
                    tl.to(curretPage, 0.2, { top: -(scrollTop - scrollDif)})
                }


                $('html, body').scrollTop($(window).scrollTop());


                $('body').css('height', $('body .wrapper').get(0).scrollHeight+ 'px');

                $('section').each((i, s) => {
                    sections_pos[i] = {}
                    sections_pos[i].el = s;
                    sections_pos[i].top = s.offsetTop
                    sections_pos[i].height = s.offsetHeight + 100
                })
                $('.wrapper > section').addClass('is-page');


                scrolling()

                $(window).on('scroll',scrolling);
            }


        }());


        !(function () {


            function mainPage() {
                const page = $('#page-1');
                const tl = gsap.timeline();

                tl.fromTo(page.find('h1'), 0.5, { opacity: 0, y: 100 }, {opacity: 1, y: 0});
                tl.fromTo(page.find('.users'), 0.5, { opacity: 0, y: 50 }, {opacity: 1, y: 0});

                tl.fromTo(page.find('.bg1'), 0.5, { opacity: 0, yPercent: -50 }, {opacity: 1, yPercent: 0},'-=0.3');
                tl.fromTo(page.find('.bg2'), 0.5, { opacity: 0, yPercent: -50 }, {opacity: 1, yPercent: 0}, '-=0.3');
                tl.fromTo(page.find('.bg3'), 0.5, { opacity: 0, yPercent: -50 }, {opacity: 1, yPercent: 0}, '-=0.3');


                gsap.to(page.find('.bg1, .bg2, .bg3'), {
                    y: '-=300',
                    height: '+=300',
                    scrollTrigger: {
                        trigger: page,
                        start: 'top top',
                        end:'+=100%',
                        scrub: true
                    }
                })
            }
            mainPage();



            $('.article-info-list').each((index, list) => {
                const tl = gsap.timeline({
                    scrollTrigger: {
                        trigger: list,
                        start: "top 90%",
                        end: "+=500px",
                        scrub: true,
                    }
                });

                tl.from($(list).find('.article-info'),1, { opacity: 0 });
                tl.from($(list).find('.article-info__caption'), 1, {x: 150}, '-=1')
                tl.from($(list).find('.article-info__numbers'), 1, {x: 200}, '-=1')
                tl.from($(list).find('.article-info__desc'), 1, {x: 250 }, '-=1')
            })

            $('.comment').each((index, comment) => {
                const tl = gsap.timeline({
                    scrollTrigger: {
                        trigger: comment,
                        start: "top 90%", end: "+=500",
                        scrub: true,
                    }
                })

                tl.from($(comment).find('.before'), 1, { opacity: 0, y: 50})
                tl.from($(comment).find('.text'), 1, { opacity: 0, y: 50, delay: 0.1})
                tl.from($(comment).find('.after'), 1, { opacity: 0, y: 50, delay: 0.1})

            })

            $('.slider-grid').each((index, sl) => {
                const slider = $(sl).find('.photo-block, .photo-slider');
                const slNav = $(sl).find('.slider-nav');
                const slDesc = $(sl).find('.controls .desc');
                const slDescPlus = $(sl).find('.controls .desc + *');
                const sliderIMG = $(sl).find('img');

                const tl = gsap.timeline({
                    scrollTrigger: {
                        trigger: sl,
                        start: "top " + ($d(560) ? '70%' : '100%'), end: "+=500",
                        scrub: true,
                    }
                })

                tl.from(slider, 3, { opacity: 0, x: 200})
                tl.from(slNav, 1, { opacity: 0 }, '-=1')
                tl.from(slDesc, 1, { opacity: 0 }, '-=1')
                tl.from(slDescPlus, 1, { opacity: 0}, '-=1')

            })

            $('.user-comment').each((index, comment) => {
                const user = $(comment).find('.user');
                const textBefore = $(comment).find('.user-comment__info .before');
                const textAfter = $(comment).find('.user-comment__info .after');
                const text = $(comment).find('.user-comment__info .text');

                const tl = gsap.timeline({
                    scrollTrigger: {
                        trigger: comment,
                        start: "top " + ($d(560) ? '90%' : '100%'), end: "+=500",
                        scrub: true,
                    }
                })

                tl.from(textBefore, 1, { opacity: 0, y: 50})
                tl.from(text, 1, { opacity: 0, y: 50, delay: 0.1 })
                tl.from(textAfter, 1, { opacity: 0, y: 50, delay: 0.1 })
                tl.from(user, 1, { opacity: 0}, '-=0.5')

            })

        }());
    }());
});

$(document).ready(() => {
    SmoothScroll({ stepSize: 55, animationTime: 500 });


    function scrolling(scrollTop) {

        if (scrollTop > 200) {
            $('.header').addClass('header_scroll')
        } else {
            $('.header').removeClass('header_scroll')
        }

    }

    scrolling($(window).scrollTop());

    $(window).on('scroll', (e) => {
        const scrollTop = $(window).scrollTop();
        scrolling(scrollTop);
    })


    $('.slider-grid .slider-nav button').on('click', (e) =>   {
        e.preventDefault()
        const slider = $(e.target).parents('.slider-grid').find('.owl-carousel');

        if ($(e.currentTarget).is('.prev')) {
            slider.trigger('prev.owl.carousel')
        } else {
            slider.trigger('next.owl.carousel')
        }
    })





    $('.header__burger').on('click', (e) => {

        if ($('.menu').hasClass('.is-open')) {
            $('.menu').stop().fadeOut();
        } else {
            $('.menu').stop().fadeIn();
        }

        $('.menu').toggleClass('is-open');

    })


    $('.menu__close').on('click', (e) => {
        $('.menu').removeClass('is-open').stop().fadeOut();
    });


    $('.header__searchbtn').on('click', (e) => $('.header__search').addClass('show'));
    $('.search__close-js').on('click', (e) => $('.header__search').removeClass('show'))

})
