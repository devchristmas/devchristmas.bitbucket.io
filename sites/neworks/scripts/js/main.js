/* 
 © Copyright Neworks Ltd
*/

function isIeEdge () {
  let userAgent = window.navigator.userAgent;
  let is_ie = userAgent.match(/{ie|edge|trident|MSIE}?/gi);
  return !!is_ie;
}
function isIe () {
  let userAgent = window.navigator.userAgent;
  let is_ie = userAgent.match(/{ie|trident|MSIE}?/gi);
  return !!is_ie;
}

// Nav overlay values - https://easings.net/
  $('#hamburger,.hamburger').on('click',function(e){
    e.preventDefault();
    let nav = $('#nav-menu');
    if (nav.is('.nav-is-open')) {
      nav.find('.close').stop().fadeOut(100);
      nav.find('.hamb').stop().fadeIn(400);
      nav.find('.nav-menu-list').stop().fadeOut(400);
    } else {
      nav.find('.hamb').stop().fadeOut(100);
      nav.find('.close').stop().fadeIn(400);
      nav.find('.nav-menu-list').stop().fadeIn(400);
    }
    nav.toggleClass('nav-is-open')
  
    $('body').toggleClass('js-no-scrolling');
  });




//  Nav bar sticky
const topMenuPos = $('#nav-menu').offset().top;
let scrollPos = 0;
function navSticky () {
  let nav = $('#nav-menu');
  let scrollTop = $(window).scrollTop();
  if (scrollTop > scrollPos) {
    nav.addClass('hidden');
  } else {
    nav.removeClass('hidden').addClass('js-scrolled');
  }
  if (scrollTop === 0) {
    nav.removeClass('js-scrolled');
  }

  scrollPos = scrollTop;
}
if (!isIeEdge()) {
  $(window).on('scroll', (e) => {
    navSticky();
  });
}


// on page load  

$(document).ready(function() {
	
	let fadeSpeed = 400;
	let animEasing = 'easeOutCubic';

	// Page fade in values
	$("#main-load").delay(fadeSpeed).fadeOut(fadeSpeed, animEasing, function() {
			$('html, body').css('overflowY', 'auto'); 
	});


	if(!isIeEdge()) {
	  // Tile card animation
    VanillaTilt.init(document.querySelectorAll(".project-card"), {
      reverse:            false,  // reverse the tilt direction
      max:                15,     // max tilt rotation (degrees)
      perspective:        1000,   // Transform perspective, the lower the more extreme the tilt gets.
      scale:              1.03,      // 2 = 200%, 1.5 = 150%, etc..
      speed:              5000,    // Speed of the enter/exit transition
      transition:         true,   // Set a transition on enter/exit.
      axis:               null,   // What axis should be disabled. Can be X or Y.
      reset:              true,   // If the tilt effect has to be reset on exit.
      easing:             "cubic-bezier(.03,.98,.52,.99)",    // Easing on enter/exit.
      glare:              false,   // if it should have a "glare" effect
      "max-glare":        0.2,      // the maximum "glare" opacity (1 = 100%, 0.5 = 50%)
      "glare-prerender":  false   // false = VanillaTilt creates the glare elements for you, otherwise
      // you need to add .js-tilt-glare>.js-tilt-glare-inner by yourself
    });
  }

		
});


