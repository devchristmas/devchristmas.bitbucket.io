// Modals
!(function ($) {

    $('[open-modal]').on('click', (e) => {
        e.preventDefault();
        const modalId = $(e.target).attr('open-modal') || false;
        if (modalId) {
            $('.ui-modal').fadeOut().removeClass('js-active');
            $(modalId).fadeIn().css('display', 'flex').addClass('js-active');
        }
    });

    $(document).on('click', (e) => {
        if ($(e.target).is('.ui-modal-close')) {
            $('.ui-modal').fadeOut().removeClass('js-active');
        }
    });

}(jQuery));


!(function ($) {

    let isOpen = false;

    const hamburger = $('#hamburger');
    const mobileMenu = $('.mobile-menu');
    const pageScrolling = $('html, body');

    hamburger.on('click', (e) => {
        if (!isOpen) {
            mobileMenu.fadeIn();
        } else {
            mobileMenu.fadeOut();
        }
        mobileMenu.toggleClass('js-active');
        pageScrolling.toggleClass('js-no-scrolling');

        isOpen = !isOpen;
    });

    mobileMenu.find('a').on('click', (e) => {
        isOpen = false;
        mobileMenu.fadeOut();
        pageScrolling.removeClass('js-no-scrolling');
    })

}(jQuery));


// Video slider
const videoSlider = $('#video-slider').flipster({
    itemContainer: 'ul',
    itemSelector: 'li',
    loop: true,
    start: 'center',
    scrollwheel: false,
    style: 'flat'
});
$('#video-slider-prev').on('click', (e) => {
    e.preventDefault();
    videoSlider.flipster('prev');
});
$('#video-slider-next').on('click', (e) => {
    e.preventDefault();
    videoSlider.flipster('next');
});


// Smooth scroll on anchor
$('a').on('click', (e) => {
    const link = $(e.target).attr('href');
    const isElement = (link !== '#' && $(link).length > 0);

    if (isElement) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $(link).offset().top
        }, 400);
    }
});


//Google map API
ymaps.ready(function () {
    const myMap = new ymaps.Map('map', {
        center: [45.033986, 38.976777],
        zoom: 15,
        controls: ['smallMapDefaultSet']
    });

    const myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        hintContent: 'reQueen',
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: 'images/map-marker.png',
        // Размеры метки.
        iconImageSize: [55, 85],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-25, -85]
    })
    myMap.geoObjects
        .add(myPlacemark)
});